(function() {
    'use strict';

    var fs = require('fs');

    function jsVendorSource(file) {
        return '../js-vendor/' + file;
    };

    function compiledSoySource(file) {
        return '../../.tmp/compiled-soy/' + file;
    };

    var config = {
        dist: {
            options: {
                map: {
                    tether: 'tether/tether'
                },
                baseUrl: 'src/js',
                preserveLicenseComments: false,
                optimize: 'none',
                skipDirOptimize: true,
                dir: '.tmp/requirejs-optimizer',
                onBuildWrite: function (name, path, content) {
                    fs.writeFileSync(path, content);
                    return content;
                },
                modules: [{
                    name: '../../.tmp/amd-stubs/aui',
                    include: [
                        'aui/internal/skate',
                        'aui/internal/tether',
                        'jquery.os',
                        'jquery.moveto',
                        'format',
                        'internal/widget',
                        'internal/alignment',
                        'internal/browser',
                        'internal/deprecation',
                        'aui.properties',
                        'layer-manager',
                        'layer-manager-global',
                        'focus-manager',
                        'layer',
                        'dialog2',
                        'cookie',
                        'dialog',
                        'dropdown2',
                        'event',
                        'forms',
                        'inline-dialog',
                        'inline-dialog2',
                        'keyCode',
                        'messages',
                        'tabs',
                        'template',
                        'trigger',
                        'whenitype',
                        'aui-header-responsive',
                        'aui/internal/fancy-file-input',
                        'aui-sidebar'
                    ],
                    exclude: [
                        'jquery',
                        'jquery-migrate'
                    ]
                }, {
                    name: '../../.tmp/amd-stubs/aui-datepicker',
                    include: [
                        'aui-date-picker'
                    ],
                    exclude: [
                        '../../.tmp/amd-stubs/aui',
                        '../../.tmp/amd-stubs/aui-experimental',
                        'aui',
                        'jquery',
                        'jquery-migrate'
                    ]
                }, {
                    name: '../../.tmp/amd-stubs/aui-experimental',
                    include: [
                        'tipsy',
                        'aui-experimental-expander',
                        'aui-experimental-progress-indicator',
                        'aui-experimental-tables-sortable',
                        'aui-experimental-tooltip',
                        'form-validation',
                        'form-provided-validators',
                        'aui-select2',
                        'dialog2',
                        'inline-dialog2',
                        'aui/internal/spin',
                        'aui-sidebar',
                        'flags'
                    ],
                    exclude: [
                        '../../.tmp/amd-stubs/aui',
                        '../../.tmp/amd-stubs/aui-datepicker',
                        'jquery',
                        'jquery-migrate'
                    ]
                }, {
                    name: '../../.tmp/amd-stubs/aui-soy',
                    include: [
                        'soy/aui',
                        'soy/badges',
                        'soy/buttons',
                        'soy/dialog2',
                        'soy/dropdown',
                        'soy/dropdown2',
                        'soy/expander',
                        'soy/form',
                        'soy/group',
                        'soy/icons',
                        'soy/inline-dialog2',
                        'soy/lozenges',
                        'soy/labels',
                        'soy/message',
                        'soy/page',
                        'soy/panel',
                        'soy/progress-tracker',
                        'soy/table',
                        'soy/tabs',
                        'soy/toolbar',
                        'soy/toolbar2',
                        'soy/trigger',
                        'soy/avatar'
                    ],
                    exclude: [
                        '../../.tmp/amd-stubs/aui',
                        '../../.tmp/amd-stubs/aui-experimental',
                        'jquery',
                        'jquery-migrate'
                    ]
                }],
                skipModuleInsertion: true
            }
        },
        options: {
            baseUrl: '/src/js',
            paths: {
                // Needed for flatpack TODO: find a better way...
                'aui.properties': '../../.tmp/aui.properties',

                // jQuery 1.x and 2.x
                'jquery': [
                    '../../bower_components/jquery/jquery',
                    '../../bower_components/jquery/dist/jquery'
                ],

                // Required for jQuery 2.x
                'jquery-migrate': '../../bower_components/jquery-migrate/jquery-migrate',

                // Skate for web components
                'aui/internal/skate': '../../bower_components/skate/dist/skate',

                'aui/internal/fancy-file-input': '../../bower_components/fancy-file-input/dist/fancy-file-input',

                'aui/internal/tether': '../../bower_components/tether/tether',

                // Unpacked soy dep
                'soyutils': '../../bower_components/soyutils/js/soyutils',

                // Compiled soy
                'soy/aui': compiledSoySource('atlassian'),
                'soy/avatar':compiledSoySource('avatar'),
                'soy/badges': compiledSoySource('badges'),
                'soy/buttons': compiledSoySource('buttons'),
                'soy/dialog2': compiledSoySource('dialog2'),
                'soy/dropdown': compiledSoySource('dropdown'),
                'soy/dropdown2': compiledSoySource('dropdown2'),
                'soy/expander': compiledSoySource('expander'),
                'soy/form': compiledSoySource('form'),
                'soy/group': compiledSoySource('group'),
                'soy/icons': compiledSoySource('icons'),
                'soy/inline-dialog2': compiledSoySource('inline-dialog2'),
                'soy/lozenges': compiledSoySource('lozenges'),
                'soy/labels': compiledSoySource('labels'),
                'soy/message': compiledSoySource('message'),
                'soy/page': compiledSoySource('page'),
                'soy/panel': compiledSoySource('panel'),
                'soy/progress-tracker': compiledSoySource('progress-tracker'),
                'soy/table': compiledSoySource('table'),
                'soy/tabs': compiledSoySource('tabs'),
                'soy/toolbar': compiledSoySource('toolbar'),
                'soy/toolbar2': compiledSoySource('toolbar2'),
                'soy/trigger': compiledSoySource('trigger'),

                // Vendors
                'underscore': jsVendorSource('underscorejs/underscore'),
                'backbone': jsVendorSource('backbone/backbone'),
                'jquery.form': jsVendorSource('jquery/plugins/jquery.form'),
                'jquery.aop': jsVendorSource('jquery/plugins/jquery.aop'),
                'jquery.ui.datepicker':jsVendorSource('jquery/jquery-ui/jquery.ui.datepicker'),
                'serializetoobject': jsVendorSource('jquery/serializetoobject'),
                'jquery-select2': jsVendorSource('jquery/plugins/jquery.select2'),
                'jquery.tablesorter': jsVendorSource('jquery/jquery.tablesorter'),
                'jquery-compatibility': jsVendorSource('jquery/jquery-compatibility'),
                'modernizr-touch': jsVendorSource('modernizr/modernizr-touch'),
                'raf': jsVendorSource('raf/raf'),  // requestAnimationFrame shim.
                'aui/internal/spin': jsVendorSource('spin/spin'),
                'tipsy': jsVendorSource('jquery/jquery.tipsy'),

                // TODO: replace this with actual jquery ui deps
                'jquery-ui': jsVendorSource('jquery/jquery-ui/jquery-ui'),

                // Jquery stuff that isn't under vendor
                'jquery.os': 'jquery/jquery.os',
                'jquery.moveto': 'jquery/jquery.moveto',

                // AUI
                'aui': 'atlassian',
                'sidebar': 'aui-sidebar',
                'jquery.hotkeys': 'jquery/jquery.hotkeys',

                // Deprecation
                'internal/deprecation': 'internal/deprecation',

                // Misc - this stuff is required by AUI old flatpack, can remove after 6.0
                'raphael.shadow': 'raphael/raphael.shadow',

                // test deps
                'aui-qunit': '../../tests/unit/aui-qunit'
            },
            shim: {
                // vendor
                'backbone': ['jquery', 'underscore'],
                'jquery-migrate': ['jquery'],
                'jquery-ui': ['jquery'],
                'jquery.aop': ['jquery'],
                'jquery.hotkeys': ['jquery'],
                'jquery.moveto': ['jquery'],
                'tipsy': ['jquery'],
                'serializetoobject': ['jquery'],
                'jquery-select2': ['jquery'],

                // aui
                'aui': {
                    deps: ['jquery', 'jquery-migrate', 'polyfills/custom-event', 'internal/deprecation'],
                    exports: 'AJS'
                },
                'aui-navigation': {
                    deps: ['aui', 'jquery', 'aui/internal/skate', 'internal/widget'],
                    exports: 'AJS.navigation'
                },
                'blanket': ['aui'],
                'aui-date-picker': ['aui', 'jquery.ui.datepicker', 'inline-dialog'],
                'jquery.ui.datepicker': ['jquery'],
                'dialog': ['aui', 'blanket', 'event'],
                'dialog2': {
                    deps: ['aui', 'jquery', 'layer', 'internal/widget', 'soy/dialog2'],
                    exports: 'AJS.dialog2'
                },
                'dropdown': ['aui', 'jquery.aop'],
                'dropdown2': ['aui'],
                'event': ['aui'],
                'experimental-events/events': ['aui'],
                'firebug': ['aui'],
                'format': ['aui'],
                'forms': ['aui'],
                'focus-manager': ['aui'],
                'inline-dialog': ['aui'],
                'internal/browser': {
                    deps: ['jquery', 'aui'],
                    exports: 'AJS._internal.browser'
                },
                'internal/deprecation': ['jquery'],
                'internal/widget': {
                    deps: ['aui', 'jquery'],
                    exports: 'AJS._internal.widget'
                },
                'keyCode': ['aui'],
                'layer': {
                    deps: ['aui', 'jquery', 'internal/widget'],
                    exports: 'AJS.layer'
                },
                'layer-manager': {
                    deps: ['aui', 'blanket', 'layer'],
                    exports: 'AJS.LayerManager'
                },
                'layer-manager-global': {
                    deps: ['aui', 'keyCode', 'layer-manager'],
                    exports: 'AJS.LayerManager.global'
                },
                'messages': ['aui', 'template'],
                'aui-select2': ['aui', 'jquery-select2'],
                'aui-sidebar': ['aui', 'raf', 'modernizr-touch', 'internal/widget', 'aui-navigation'],
                'aui-experimental-tables-sortable': ['aui', 'jquery.tablesorter'],
                'tabs': ['aui'],
                'template': {
                    deps: ['aui'],
                    exports: 'AJS.template'
                },
                'toolbar': ['aui'],
                'aui-experimental-tooltip': ['aui'],
                'whenitype': ['aui', 'format', 'keyCode', 'dropdown', 'jquery.hotkeys', 'jquery.moveto'],
                'experimental-autocomplete/progressive-data-set': ['aui', 'backbone'],
                'experimental-autocomplete/query-input': ['aui', 'backbone'],
                'experimental-autocomplete/query-result': ['aui', 'backbone'],
                'experimental-autocomplete/truncating-progressive-data-set': ['aui', 'experimental-autocomplete/progressive-data-set'],
                'experimental-restfultable/restfultable': ['aui', 'backbone', 'experimental-events/events', 'format'],
                'experimental-restfultable/restfultable.entrymodel': ['aui', 'backbone', 'experimental-events/events', 'experimental-restfultable/restfultable'],
                'experimental-restfultable/restfultable.row': ['aui', 'backbone', 'experimental-restfultable/restfultable', 'soy/icons'],
                'experimental-restfultable/restfultable.editrow': ['aui', 'backbone', 'experimental-restfultable/restfultable', 'serializetoobject'],
                'experimental-restfultable/restfultable.customview': ['aui', 'backbone', 'experimental-restfultable/restfultable'],

                // restful table and its children have circular deps (restful table uses restfultable.*, restfultable.* requires
                // the restfultable namespace, so create a finaldep
                'experimental-restfultable/restfultable.all': ['experimental-restfultable/restfultable', 'experimental-restfultable/restfultable.entrymodel', 'experimental-restfultable/restfultable.row', 'experimental-restfultable/restfultable.editrow', 'experimental-restfultable/restfultable.customview'],

                // soy
                'soy/aui': ['soyutils'],
                'soy/avatar': ['soyutils', 'soy/aui'],
                'soy/badges': ['soyutils', 'soy/aui'],
                'soy/buttons': ['soyutils', 'soy/aui'],
                'soy/dialog2': ['soyutils', 'soy/aui'],
                'soy/dropdown': ['soyutils', 'soy/aui'],
                'soy/dropdown2': ['soyutils', 'soy/aui'],
                'soy/expander': ['soyutils', 'soy/aui'],
                'soy/form': ['soyutils', 'soy/aui'],
                'soy/group': ['soyutils', 'soy/aui'],
                'soy/icons': ['soyutils', 'soy/aui'],
                'soy/inline-dialog2': ['soyutils', 'soy/aui'],
                'soy/lozenges': ['soyutils', 'soy/aui'],
                'soy/labels': ['soyutils', 'soy/aui'],
                'soy/message': ['soyutils', 'soy/aui'],
                'soy/pane': ['soyutils', 'soy/aui'],
                'soy/panel': ['soyutils', 'soy/aui'],
                'soy/progress-tracker': ['soyutils', 'soy/aui'],
                'soy/table': ['soyutils', 'soy/aui'],
                'soy/tabs': ['soyutils', 'soy/aui'],
                'soy/toolbar': ['soyutils', 'soy/aui'],
                'soy/toolbar2': ['soyutils', 'soy/aui'],
                'soy/trigger': ['soyutils', 'soy/aui'],

                // test
                'aui-qunit': ['aui', 'polyfills/custom-event', 'layer-manager-global']
            }
        }
    };


    // The r.js optimiser doesn't use path fallbacks so we must detect which
    // version we are using and map it to that version.
    config.options.paths.jquery.some(function(path) {
        if (fs.existsSync('.' + config.options.baseUrl + '/' + path + '.js')) {
            config.options.paths.jquery = path;
            return true;
        }
    });


    module.exports = config;
}());
