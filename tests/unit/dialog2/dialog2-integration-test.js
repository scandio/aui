define(['jquery', 'dialog2', 'layer', 'soy/dialog2', 'aui-qunit'], function($, dialog2Widget, layerWidget) {
    'use strict';
    /*jshint quotmark:false */

    function onEvent(dialog, event, fn) {
        dialog.addEventListener('aui-layer-' + event, fn);
    }

    function offEvent(dialog, event, fn) {
        dialog.removeEventListener('aui-layer-' + event, fn);
    }

    function createDialog() {
        return $(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
    }

    module("Dialog2 integration tests", {
        setup: function() {
            sinon.spy(AJS, "dim");
            sinon.spy(AJS, "undim");
        },
        teardown: function() {
            AJS.dim.restore();
            AJS.undim.restore();
            // AJS.layer.show() moves the element to be a child of the body, so clean up popups
            $(".aui-layer").remove();
        },
        pressEsc: function() {
            AJS.qunit.pressKey(AJS.keyCode.ESCAPE);
        },
        createContentEl: function() {
            return $(aui.dialog.dialog2({
                content: "Hello world"
            })).appendTo("#qunit-fixture");
        },
        createInputContentEl: function () {
            return $(aui.dialog.dialog2({
                headerActionContent: "<button id='my-button'>Button</button>",
                content: "<input type='text' id='my-input' />",
                footerActionContent: "<button id='footer-button'>Footer button</button>"
            })).appendTo("#qunit-fixture");
        },
        clickBlanket: function() {
            // We don't want to include blanket.js - which creates the blanket - in our dependencies,
            // so create a mock blanket element
            var $blanket = $("<div></div>").addClass("aui-blanket").appendTo("#qunit-fixture");
            $blanket.click();
        }
    });

    test("Dialog2 focuses inside panel not on buttons", function() {
        var $el = $(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "<input type='text' id='my-input' />"
        })).appendTo("#qunit-fixture");
        var buttonFocusSpy = sinon.spy();
        $("#my-button").focus(buttonFocusSpy);
        var inputFocusSpy = sinon.spy();
        $("#my-input").focus(inputFocusSpy);

        var dialog = dialog2Widget($el);
        dialog.show();

        equal(buttonFocusSpy.callCount, 0, "Expected my-button to never be focused");
        ok(inputFocusSpy.calledOnce, "Expected my-input to be focused");
    });

    test('Dialog focuses header if there are no other elements', function() {
        var $el = $(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "Some content (nothing you can focus)"
        })).appendTo("#qunit-fixture");
        var buttonFocusSpy = sinon.spy();
        $("#my-button").focus(buttonFocusSpy);

        var dialog = dialog2Widget($el);
        dialog.show();

        equal(buttonFocusSpy.callCount, 1, "Expected my-button to be focused");
    });

    test("Dialog2 creates a blanket", function() {
        var $el = this.createInputContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        ok(AJS.dim.calledOnce, "Expected blanket to be shown");
    });

    test("Dialog2 remove on hide false", function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        dialog.show();

        dialog.hide();

        ok($el.parent().length);
    });

    test("Dialog2 remove on hide true", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");
        var dialog = dialog2Widget($el);
        dialog.show();

        dialog.hide();

        ok(!$el.parent().length);
    });

    test("Dialog2 instance events on", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);

        var spy = sinon.spy();

        dialog.on("hide", spy);
        dialog.show();
        dialog.hide();

        ok(spy.calledOnce, "Expected event listener called");
    });

    test("Dialog2 instance show events on native", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);

        var spy = sinon.spy();

        onEvent($el[0], 'show', spy);
        dialog.show();
        offEvent($el[0], 'show', spy);

        ok(spy.calledOnce, "Expected event listener called");
    });

    test("Dialog2 instance hide events on native", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);

        var spy = sinon.spy();

        onEvent($el[0], 'hide', spy);
        dialog.show();
        dialog.hide();
        offEvent($el[0], 'hide', spy);

        ok(spy.calledOnce, "Expected event listener called");
    });

    test("Dialog2 instance events can be turned off", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);

        var spy = sinon.spy();

        dialog.on("hide", spy);
        dialog.off("hide", spy);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Dialog2 instance events can be turned off native", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);

        var spy = sinon.spy();

        onEvent($el[0], 'hide', spy);
        offEvent($el[0], 'hide', spy);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test('Dialog2 global show event', function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        dialog2Widget.on('show', spy);
        dialog.show();
        dialog2Widget.off('show', spy);

        ok(spy.calledOnce, 'Expected event listener called');
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], 'Expected first arg was dialog element');
    });


    test('Dialog2 global show event native', function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        onEvent(document, 'show', spy);
        dialog.show();
        offEvent(document, 'show', spy);

        ok(spy.calledOnce, 'Expected event listener called');
    });

    test('Dialog2 global show event not triggered by normal show events', function() {
        var $el = this.createContentEl();
        var spy = sinon.spy();

        dialog2Widget.on('show', spy);
        $el.trigger('show');
        dialog2Widget.off('show', spy);

        ok(spy.notCalled, 'Expected event listener not called');
    });

    test('Dialog2 global show event not triggered by normal show events on innerElement', function() {
        var $el = this.createContentEl();
        var $innerEl = $('<span class="inner-component"></span>');
        $el.append($innerEl);
        var spy = sinon.spy();

        dialog2Widget.on('show', spy);
        $innerEl.trigger('show');
        dialog2Widget.off('show', spy);

        ok(spy.notCalled, 'Expected event listener not called');
    });

    test("Dialog2 global hide event", function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        dialog2Widget.on("hide", spy);
        dialog.show();
        dialog.hide();
        dialog2Widget.off("hide", spy);

        ok(spy.calledOnce, "Expected event listener called");
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], "Expected first arg was dialog element");
    });

    test("Dialog2 global hide event native", function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        onEvent(document, 'hide', spy);
        dialog.show();
        dialog.hide();
        offEvent(document, 'hide', spy);

        ok(spy.calledOnce, "Expected event listener called");
    });

    test("Dialog2 global events can be turned off", function() {
        var $el = $(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        dialog2Widget.on("hide", spy);
        dialog2Widget.off("hide", spy);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Dialog2 global events can be turned off native", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();

        onEvent(document, 'hide', spy);
        offEvent(document, 'hide', spy);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Dialog2 two global events turned off correctly, same event name", function() {
        var $el = $(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        dialog2Widget.on("hide", spy);
        dialog2Widget.on("hide", spy2);
        dialog2Widget.off("hide", spy);
        dialog.show();
        dialog.hide();
        dialog2Widget.off("hide", spy2);

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.calledOnce, "Expected event listener called spy2");
    });

    test("Dialog2 two global events turned off correctly, same event name native", function() {
        var $el = createDialog();
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        onEvent(document, 'hide', spy);
        onEvent(document, 'hide', spy2);
        offEvent(document, 'hide', spy);
        dialog.show();
        dialog.hide();
        offEvent(document, 'hide', spy2);

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.calledOnce, "Expected event listener called spy2");
    });

    test("Dialog2 two global events turned off correctly, different event names", function() {
        var $el = $(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        dialog2Widget.on("hide", spy);
        dialog2Widget.on("show", spy2);
        dialog2Widget.off("hide", spy);
        dialog2Widget.off("show", spy2);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.notCalled, "Expected event listener called spy2");
    });

    test("Dialog2 two global events turned off correctly, different event names native", function() {
        var $el = $(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = dialog2Widget($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        onEvent(document, 'hide', spy);
        onEvent(document, 'show', spy2);
        offEvent(document, 'hide', spy);
        offEvent(document, 'show', spy2);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.notCalled, "Expected event listener called spy2");
    });

    test("Dialog2 global beforeHide event cancels hide", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);
        var stub = sinon.stub().returns(false);

        dialog2Widget.on("beforeHide", stub);
        dialog.show();
        dialog.hide();
        dialog2Widget.off("beforeHide", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(layerWidget($el).isVisible(), "Expected first element hidden");
    });

    test('Dialog2 Global hide event cancels hide native', function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);

        var callCount = 0;
        var cancelEvent = function(e) {
            callCount++;
            e.preventDefault();
        }

        onEvent(document, 'hide', cancelEvent);
        dialog.show();
        dialog.hide();
        offEvent(document, 'hide', cancelEvent);

        ok(callCount == 1, "Expected bound handler was not called");
        ok(layerWidget($el).isVisible(), "Expected first element hidden");
    });

    test("Dialog2 global beforeShow event cancels show", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);
        var stub = sinon.stub().returns(false);

        dialog2Widget.on("beforeShow", stub);
        dialog.hide();
        dialog.show();
        dialog2Widget.off("beforeShow", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(!layerWidget($el).isVisible(), "Expected first element hidden");
    });

    test("Dialog2 global show event cancels show native", function() {
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);

        var callCount = 0;
        var cancelEvent = function(e) {
            callCount++;
            e.preventDefault();
        }

        onEvent(document, 'show', cancelEvent);
        dialog.hide();
        dialog.show();
        offEvent(document, 'show', cancelEvent);

        ok(callCount == 1, "Expected bound handler was not called");
        ok(!layerWidget($el).isVisible(), "Expected first element hidden");
    });

    test("Dialog2 does not remove on hide when removeOnHide is false", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();
        dialog.hide();

        ok($el.parent().length, "Dialog2 not removed when hide is called");
    });

    test("Dialog2 removes on hide when removeOnHide is true", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = dialog2Widget($el);
        dialog.show();
        dialog.hide();

        ok(!$el.parent().length, "Dialog2 removed when hide is called");
    });

    test("Dialog2 data-aui-remove-on-hide=false doesn't remove on escape", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        this.pressEsc();

        ok($el.parent().length, "Dialog2 not removed when escape is pressed");
    });

    test("Dialog2 data-aui-remove-on-hide=true removes on escape", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = dialog2Widget($el);
        dialog.show();

        this.pressEsc();

        ok(!$el.parent().length, "Dialog2 removed when escape is pressed");
    });

    test("Dialog2 data-aui-remove-on-hide=false doesn't remove on blanket click", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);
        dialog.show();

        this.clickBlanket();

        ok($el.parent().length, "Dialog2 not removed when blanked clicked");
    });

    test("Dialog2 data-aui-remove-on-hide=true removes on blanket click", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = dialog2Widget($el);
        dialog.show();

        this.clickBlanket();
        ok(!$el.parent().length, "Dialog2 removed when blanket clicked");
    });

});