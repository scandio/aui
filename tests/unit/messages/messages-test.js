define(['messages', 'aui-qunit'], function() {
    'use strict';
    /*jshint quotmark:false */

    module("Messages Unit Tests", {
        setup : function() {
            AJS.$('#qunit-fixture').html('<div id="aui-message-bar"></div>');
            this.messagebar = AJS.$("#aui-message-bar");
            this.clock = sinon.useFakeTimers();
        },
        teardown: function() {
            this.clock.restore();
        }
    });

    function createMessageWithID(testid) {
        AJS.messages.info({
            id: testid,
            title: "Title",
            body: "This message was created by messagesSetup() with id " + testid
        });
    }

    function checkNoID(target) {
        return {
            found: target.find(".aui-message")[0].getAttribute("id"),
            expected: null
        };
    }

    test("Messages API", function() {
        ok(typeof AJS.messages == "object", "AJS.messages exists");
        ok(typeof AJS.messages.setup == "function", "AJS.messages.setup exists");
        ok(typeof AJS.messages.makeCloseable == "function", "AJS.messages.makeCloseable exists");
        ok(typeof AJS.messages.template == "string", "AJS.messages.template exists");
        ok(typeof AJS.messages.createMessage == "function", "AJS.messages.createMessage exists");
    });

    test("Messages ID test: bad ID", function() {
        createMessageWithID("#t.e.st-m### e s s a '''\"\"g e-id-full-of-dodgy-crap");
        var checkedNoID = checkNoID(this.messagebar);
        equal( checkedNoID.found, checkedNoID.expected, "There should be no ID (bad ID supplied)");
    });

    test("Messages ID test: no ID", function() {
        createMessageWithID();
        var checkedNoID = checkNoID(this.messagebar);
        equal( checkedNoID.found, checkedNoID.expected, "There should be no ID (no ID supplied)");
    });

    test("Messages ID test: good ID", function() {
        createMessageWithID("test-id");
        ok( AJS.$("#test-id").length, "#test-id should be found" );
    });

    test('Closeable messages get a close button', function() {
        AJS.messages.info({
            id: 'close-message-test',
            title: 'Title',
            body: 'This message can be closed',
            closeable: true
        });

        strictEqual(AJS.$('#close-message-test').find('.icon-close').length, 1, 'There is exactly one close button in the closeable message');
    });

    test('Closing a message triggers the document aui-close-message event', function() {
        AJS.messages.info({
            id: 'close-message-test',
            title: 'Title',
            body: 'This message can be closed',
            closeable: true
        });

        strictEqual(AJS.$('#close-message-test').length, 1, 'Message is open before clicking the close button');

        var closeMessageHandler = sinon.spy();
        $(document).on('aui-message-close', closeMessageHandler);

        AJS.$('#close-message-test .icon-close').click();
        this.clock.tick(100);

        strictEqual(AJS.$('#close-message-test').length, 0, 'Message is closed after clicking the close button');
        ok(closeMessageHandler.called, 'The aui-message-close event is triggered after closing a message');
    });
});