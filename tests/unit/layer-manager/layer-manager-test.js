define(['jquery', 'layer-manager', 'aui-qunit'], function($) {
    'use strict';
    /*globals _, notEqual, notStrictEqual */

    /**
     * @returns {jQuery}
     */
    function createLayer(blanketed) {
        var $layer = $('<div>');
        $layer.appendTo('#qunit-fixture');
        if (blanketed) {
            $layer.attr('data-aui-blanketed', true);
        }
        return $layer;
    }

    /**
     * @returns {jQuery}
     */
    function createBlanketedLayer() {
        return createLayer(true);
    }

    function layers() {
        return AJS.LayerManager.global;
    }

    module('Layer Manager', {
        setup: AJS.qunit.removeLayers,
        teardown: AJS.qunit.removeLayers
    });

    test('push() accepts jQuery elements', function() {
        var $layer1 = createLayer();
        layers().push($layer1);

        strictEqual(layers().indexOf($layer1), 0);
    });

    test('push() accepts HTML elements', function() {
        var $layer1 = createLayer();
        layers().push($layer1[0]);

        strictEqual(layers().indexOf($layer1), 0);
    });

    test('indexOf() should return the correct index', function() {
        var $layer1 = createBlanketedLayer();
        var $layer2 = createBlanketedLayer();
        var $nonExistentLayer = createLayer();

        layers().push($layer1);
        layers().push($layer2);

        strictEqual(layers().indexOf($nonExistentLayer), -1);
        strictEqual(layers().indexOf($layer1), 0);
        strictEqual(layers().indexOf($layer2), 1);
    });

    test('indexOf() accepts HTML elements', function() {
        var $layer1 = createBlanketedLayer();
        var $layer2 = createLayer();

        layers().push($layer1);
        layers().push($layer2[0]);

        strictEqual(layers().indexOf($layer1[0]), 0, 'should find HTML element even if jQuery element pushed');
        strictEqual(layers().indexOf($layer2), 1, 'should find jQuery element even if HTML element pushed');
    });

    test('item() returns a jQuery element', function() {
        var $layer1 = createLayer();
        layers().push($layer1[0]);

        ok(layers().item(0) instanceof $, 'should be a jQuery object');
    });

    test('item() should return the correct element', function() {
        var $layer1 = createBlanketedLayer();
        var $layer2 = createLayer();

        layers().push($layer1);
        layers().push($layer2);

        strictEqual(layers().item(0)[0], $layer1[0]);
        strictEqual(layers().item(1)[0], $layer2[0]);
    });

    test('item() returns same jQuery object if jQuery pushed', function() {
        var $layer1 = createBlanketedLayer();
        var $layer2 = createLayer();

        layers().push($layer1);
        layers().push($layer2[0]);

        strictEqual(layers().item(0), $layer1, 'should be the same jQuery object as pushed');
        notEqual(layers().item(1), $layer2, 'HTML element was pushed, so cannot be same jQuery object');
        // NOTE AUI-2604 - We may not actually care about the jQuery object equivalence.
    });

    module('Layer Manager - layering', {
        setup: function() {
            AJS.qunit.removeLayers();
            this.$blanketedLayerOne = createBlanketedLayer();
            this.$normalLayerOne = createLayer();
            this.$normalLayerTwo = createLayer();
            this.$normalLayerThree = createLayer();
        },
        teardown: AJS.qunit.removeLayers
    });

    // AUI-2590
    test('push() will implicitly remove other layers at the current level in the stack', function() {
        layers().push(this.$normalLayerOne);
        layers().push(this.$normalLayerTwo);

        strictEqual(layers().indexOf(this.$normalLayerOne), -1, 'first layer was removed from level');
        strictEqual(layers().indexOf(this.$normalLayerTwo), 0, 'second layer is on top level');

        layers().push(this.$normalLayerThree);

        strictEqual(layers().indexOf(this.$normalLayerOne), -1, 'first layer still gone');
        strictEqual(layers().indexOf(this.$normalLayerTwo), -1, 'second layer was removed from level');
        strictEqual(layers().indexOf(this.$normalLayerThree), 0, 'third layer is on top level');
    });

    test('push() will stack layers that have a parent-child relationship', function() {
        this.$normalLayerOne.attr('id', 'one').html('<span aria-controls="two"></span>');
        this.$normalLayerTwo.attr('id', 'two').html('<span aria-controls="three"></span>');
        this.$normalLayerThree.attr('id', 'three');

        layers().push(this.$normalLayerOne);
        layers().push(this.$normalLayerTwo);

        strictEqual(layers().indexOf(this.$normalLayerOne), 0, 'first layer still on first level');
        strictEqual(layers().indexOf(this.$normalLayerTwo), 1, 'second layer on new level');

        layers().push(this.$normalLayerThree);

        strictEqual(layers().indexOf(this.$normalLayerOne), 0, 'first layer still on first level');
        strictEqual(layers().indexOf(this.$normalLayerTwo), 1, 'second layer still on second level');
        strictEqual(layers().indexOf(this.$normalLayerThree), 2, 'third layer on new level');
    });

    test('push() will start new layer level on top of blanketed layer', function() {
        layers().push(this.$blanketedLayerOne);
        layers().push(this.$normalLayerOne);

        strictEqual(layers().indexOf(this.$blanketedLayerOne), 0, 'blanket layer is at bottom of stack');
        strictEqual(layers().indexOf(this.$normalLayerOne), 1, 'normal layer should be on top');
    });

    module('Layer Manager - layer removal', {
        setup: function() {
            AJS.qunit.removeLayers();
            this.$layerOne = createLayer();
            this.$layerTwo = createLayer();
            this.$layerThree = createLayer();
            this.parentChild(this.$layerOne, this.$layerTwo);
            this.parentChild(this.$layerTwo, this.$layerThree);

            this.$layerFour = createBlanketedLayer();
            this.parentChild(this.$layerThree, this.$layerFour);

            this.$layerFive = createLayer();
            this.$layerSix = createLayer();
            this.parentChild(this.$layerFive, this.$layerSix);

            this.$nonExistentLayer = createLayer();

            layers().push(this.$layerOne);
            layers().push(this.$layerTwo);
            layers().push(this.$layerThree);
            layers().push(this.$layerFour);
            layers().push(this.$layerFive);
            layers().push(this.$layerSix);

            strictEqual(layers().indexOf(this.$layerSix), 5, 'top-most layer should be on level 5');
        },
        teardown: function() {
            AJS.qunit.removeLayers();
        },
        parentChild: function(parentLayer, childLayer) {
            var id = _.uniqueId();
            childLayer.attr('id', id);
            parentLayer.html('<span aria-controls="' + id + '"></span>');
        }
    });

    test('popUntil() returns top-most layer after popping all non-modals', function() {
        var result = layers().popUntil(this.$layerFive);

        strictEqual(layers().indexOf(this.$layerSix), -1, 'layer6 should be removed from the stack');
        strictEqual(layers().indexOf(this.$layerFive), -1, 'layer5 should also be removed from the stack');
        strictEqual(layers().indexOf(this.$layerFour), 3, 'layer4 should be on top of stack');
        strictEqual(result[0], this.$layerFive[0], 'last layer to be popped is returned');
    });

    test('popUntil() returns null if argument is not a layer', function() {
        var result = layers().popUntil(this.$nonExistentLayer);

        equal(result, null, 'not a layer, so null is returned');
    });

    test('popUntil() returns null if layer was not on stack', function() {
        layers().popUntil(this.$layerFive);
        var result = layers().popUntil(this.$layerFive);

        equal(result, null, 'layer5 was removed, so calling popUntil() on it should return null');
    });

    test('popUntil() accepts HTML elements', function() {
        var result = layers().popUntil(this.$layerFive[0]);

        strictEqual(result[0], this.$layerFive[0], 'layer5 was on the stack, so popUntil() should return a non-null result');
    });

});
