define(['jquery', 'aui/internal/skate', 'trigger', 'aui-qunit'], function($, skate) {
    'use strict';


    var element;
    var trigger;
    var disabledTrigger;
    var clock;
    var componentAttributeFlag = 'data-my-component';
    var componentId = 'my-element';


    function click(trigger) {
        AJS.qunit.click(trigger);
    }

    function hover(trigger) {
        AJS.qunit.hover(trigger);
    }

    function focus(trigger) {
        var e = jQuery.Event('focus');
        $(trigger).trigger(e);
    }

    function createComponent() {
        return skate(componentAttributeFlag, {
            type: skate.types.ATTR,
            prototype: {
                show: function() {
                    this.style.display = 'block';
                    this.dispatchEvent(new CustomEvent('aui-after-show'));
                },
                hide: function() {
                    this.style.display = 'none';
                    this.dispatchEvent(new CustomEvent('aui-after-hide'));
                },
                isVisible: function() {
                    return this.style.display === 'block';
                }
            }
        });
    }

    function createElement() {
        var el = document.createElement('div');
        $(el)
            .text('some content')
            .attr('id', componentId)
            .attr(componentAttributeFlag, '')
            .addClass('aria-hidden', 'true')
            .css({
                display: 'none',
                height: 100,
                width: 100
            })
            .appendTo('#qunit-fixture');
        skate.init(el);

        return el;
    }

    function triggerFactory(tag, type, attributes) {
        var el = document.createElement(tag);

        if (attributes && typeof attributes === 'object') {
            for(var prop in attributes) {
                el.setAttribute(prop, attributes[prop]);
            }
        }

        el.setAttribute('data-aui-trigger', '');
        el.setAttribute('aria-controls', componentId);
        document.getElementById('qunit-fixture').appendChild(el);

        skate.init(el);

        return el;
    }

    function createButtonTrigger(type, attributes) {
        return triggerFactory('button', type, attributes);
    }

    function createAnchorTrigger(type, attributes) {
        return triggerFactory('a', type, attributes);
    }

    function disableTrigger(trigger) {
        trigger.setAttribute('aria-disabled', 'true');
        return trigger;
    }


    createComponent();


    module('Trigger - Behaviour', {
        setup: function() {
            element = createElement();

            element.message = sinon.spy();

            trigger = createButtonTrigger('toggle');
            disabledTrigger = disableTrigger(createButtonTrigger('toggle'));
            clock = sinon.useFakeTimers();
        },

        teardown: function() {
            clock.restore();
        }
    });

    test('isEnabled() should return false after aria-disabled="true" is added', function() {
        ok(trigger.isEnabled());
        disableTrigger(trigger);
        ok(!trigger.isEnabled());
    });

    test('isEnabled() should return true when there is no aria-disabled attribute', function() {
        ok(trigger.isEnabled());
    });

    test('disable() should disable the trigger', function() {
        ok(trigger.isEnabled());
        trigger.disable();
        ok(!trigger.isEnabled());
    });

    test('enable() should enable the trigger', function() {
        ok(trigger.isEnabled());
        trigger.disable();
        ok(!trigger.isEnabled());
        trigger.enable();
        ok(trigger.isEnabled());
    });

    test('component should receive click message when trigger is clicked', function() {
        click(trigger);
        ok(element.message.calledOnce);
        sinon.assert.calledWith(element.message, sinon.match.has('type', 'click'));
    });

    test('component should receive hover message when trigger is hovered', function() {
        hover(trigger);
        strictEqual(element.message.callCount, 1);
        sinon.assert.calledWith(element.message, sinon.match.has('type', 'mouseover'));
    });

    test('component should receive focus message when trigger is focused', function() {
        focus(trigger);
        strictEqual(element.message.callCount, 1);
        sinon.assert.calledWith(element.message, sinon.match.has('type', 'focus'));
    });

    test('should not toggle when disabled trigger is clicked', function() {
        click(disabledTrigger);
        ok(element.message.notCalled);
    });

    test('should not toggle when disabled trigger is hovered', function() {
        hover(disabledTrigger);
        ok(element.message.notCalled);
    });

    test('should not toggle when disabled trigger is focused', function() {
        focus(disabledTrigger);
        ok(element.message.notCalled);
    });


    module('Trigger - Elements', {
        setup: function() {
            element = createElement();
            element.message = sinon.spy();
            element.hide(); //make sure the element is hidden initially
            this.dontCallMe = sinon.stub().returns('Wait, no! Don\'t go :(');
            $(window).on('beforeunload', this.dontCallMe);
        },

        teardown: function() {
            $(window).off('beforeunload', this.dontCallMe);
            if (trigger && trigger.remove) { trigger.remove(); }
        }
    });

    test('if a trigger is an anchor, its hyperlink should not be followed', function() {
        trigger = createAnchorTrigger('toggle');
        trigger.setAttribute('href', 'http://google.com/');

        click(trigger);
        equal(this.dontCallMe.callCount, 0, 'should not follow hyperlink');
    });

});
