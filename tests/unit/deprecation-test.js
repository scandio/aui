define(['aui-qunit'], function() {
    'use strict';

    var fakeStack = [];
    fakeStack.push('Error');
    fakeStack.push('    at getDeprecatedLocation (http://localhost:9876/base/src/js/internal/deprecation.js:67:19)');
    fakeStack.push('    at http://localhost:9876/base/src/js/internal/deprecation.js:40:38');
    fakeStack.push('    at Object.<anonymous> (http://localhost:9876/base/src/js/internal/deprecation.js:91:13)');
    fakeStack.push('    at assertFunctionDeprecated (http://localhost:9876/base/tests/unit/deprecation-test.js:37:22)');
    fakeStack.push('    at Object.<anonymous> (http://localhost:9876/base/tests/unit/deprecation-test.js:70:9)');
    fakeStack.push('    at Object.Test.run (http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:203:18)');
    fakeStack.push('    at http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:361:10');
    fakeStack.push('    at process (http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:1453:24)');
    fakeStack.push('    at http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:479:5');

    function getFakeStackMessage () {
        return fakeStack[4];
    }

    module('Deprecation Util', {
        setup: function() {
            if (!console) {
                console = {
                    log: function() {}
                };
            }
            sinon.spy(console, 'log');
            sinon.stub(AJS.deprecate, '__getDeprecatedLocation', getFakeStackMessage);
        },
        teardown: function() {
            console.log.restore();
            AJS.deprecate.__getDeprecatedLocation.restore();
        }
    });

    function assertDeprecationMessageContainsInputs(message, display, alternate, since, remove, extraInfo) {
        if (!message) {
            ok(false, 'No message was logged.');
            return;
        }
        message = message.split('\n')[0];
        ok(~message.indexOf(display || 'DISPLAY'), 'Logged the given display name in message: ' + message);
        ok(~message.indexOf(alternate || 'ALTERNATE'), 'Logged the given alternate name in message: ' + message);
        ok(~message.indexOf(since || 'SINCE'), 'Logged the given since version in message: ' + message);
        ok(~message.indexOf(remove || 'REMOVE'), 'Logged the given removal version in message: ' + message);
        ok(~message.indexOf(extraInfo || 'EXTRA'), 'Logged the given extra info in message: ' + message);
    }

    function assertFunctionDeprecated(deprecatedFn, self, arg, display, alternate, since, remove, extraInfo) {
        console.log.reset();
        deprecatedFn.call(self, arg);
        ok(console.log.calledOnce, 'console.log should have been called once');
        assertDeprecationMessageContainsInputs(console.log.args[0] && console.log.args[0][0], display, alternate, since, remove, extraInfo);
        deprecatedFn.call(self, arg);
        ok(console.log.calledOnce, 'console.log should not have been called again');
    }

    function assertNoncallablePropertyDeprecated(obj, deprecatedProp, initialVal, display, alternate, since, remove, extraInfo) {
        console.log.reset();
        var actualValue = obj[deprecatedProp];
        strictEqual(actualValue, initialVal, 'accessing property should return real value');

        ok(console.log.calledOnce, 'console.log should have been called once');
        assertDeprecationMessageContainsInputs(console.log.args[0] && console.log.args[0][0], display, alternate, since, remove, extraInfo);
        strictEqual(obj[deprecatedProp] = 'a', 'a', 'setting property should return set value');
        ok(console.log.calledOnce, 'console.log should not have been called again for setting');
        strictEqual(obj[deprecatedProp], 'a', 'setting property should set real value');
        obj[deprecatedProp] = actualValue;
    }

    test('deprecate.fn', function() {
        //expect(12);

        var self = {};
        var arg = {};

        function fn(param) {
            strictEqual(this, self, 'function was called with right context');
            strictEqual(param, arg, 'function was called with right args');
        }
        var deprecatedFn = AJS.deprecate.fn(fn, 'DISPLAY', {alternativeName:'ALTERNATE', removeInVersion: 'REMOVE', sinceVersion:'SINCE', extraInfo:'EXTRA'});
        equal(typeof deprecatedFn, 'function', 'deprecate.fn returns a function');

        assertFunctionDeprecated(deprecatedFn, self, arg);
    });

    test('deprecate.prop', function() {
        //expect(21);
        var arg = {};

        var prop = {};
        var obj = {
            prop : prop,
            method : function(param) {
                strictEqual(this, obj, 'method was called with right context');
                strictEqual(param, arg, 'method was called with right args');
            }
        };

        AJS.deprecate.prop(obj, 'prop', {displayName:'DISPLAY', alternativeName:'ALTERNATE', removeInVersion: 'REMOVE', sinceVersion:'SINCE', extraInfo:'EXTRA'});
        assertNoncallablePropertyDeprecated(obj, 'prop', prop);

        AJS.deprecate.prop(obj, 'method', {displayName:'DISPLAY', alternativeName:'ALTERNATE', removeInVersion: 'REMOVE', sinceVersion:'SINCE', extraInfo:'EXTRA'});
        assertFunctionDeprecated(obj.method, obj, arg);
    });

    test('deprecate.obj', function() {
        //expect(21);
        var arg = {};

        var prop = {};
        var method = function(param) {
            strictEqual(this, obj, 'method was called with right context');
            strictEqual(param, arg, 'method was called with right args');
        };
        var obj = {
            prop : prop,
            method : method
        };

        AJS.deprecate.obj(obj, 'OBJ:', {
            alternativeNamePrefix: 'ALTERNATIVE:',
            removeInVersion: 'REMOVE',
            sinceVersion: 'SINCE',
            extraInfo: 'EXTRA INFO'
        });

        assertNoncallablePropertyDeprecated(obj, 'prop', prop, 'OBJ:prop', 'ALTERNATIVE:prop', 'SINCE', 'REMOVE', 'EXTRA INFO');
        assertFunctionDeprecated(obj.method, obj, arg, 'OBJ:method', 'ALTERNATIVE:method', 'SINCE', 'REMOVE', 'EXTRA INFO');
    });

});