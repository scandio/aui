define(['flags', 'jquery', 'internal/browser', 'aui-qunit'], function(flag, $, browser) {
    'use strict';

    module('Flag message tests', {
        setup: function() {
            sinon.stub(window, 'requestAnimationFrame', function(fn) { fn(); });
            sinon.stub(browser, 'supportsRequestAnimationFrame').returns(true);
            $('#qunit-fixture').append('<div id="aui-flag-container"></div>');
        },
        setupInfoFlag: function() {
            var title = 'Title';
            var body = 'This message should float over the screen';
            flag({
                type: 'info',
                title: title,
                body: body,
                persistent: false
            });

            return {
                title: title,
                body: body
            };
        },
        setupDefaultFlag: function() {
            flag({body: 'This is a message with nearly all options default'});
        },
        teardown: function() {
            $('#aui-flag-container').remove();
            window.requestAnimationFrame.restore();
            browser.supportsRequestAnimationFrame.restore();
        },
        getFlagText: function() {
            return $('.aui-flag');
        }
    });

    test('The floating message is present on the screen', function() {
        this.setupInfoFlag();
        strictEqual(this.getFlagText().length, 1, 'There is exactly one flag message on the screen');
    });

    test('Floating messages HTML contain the title and contents somewhere', function() {
        var flagProperties = this.setupInfoFlag();
        ok(this.getFlagText().html().indexOf(flagProperties.title) !== -1, 'The message title is present in the message');
        ok(this.getFlagText().html().indexOf(flagProperties.body) !== -1, 'The message body is present in the message');
    });

    test('Messages appear with mostly default options', function() {
        this.setupDefaultFlag();
        strictEqual(this.getFlagText().length, 1, 'There is one flag on the screen');
    });
});