define(['jquery', 'dialog2', 'layer', 'internal/browser', 'internal/widget', 'soy/dialog2', 'aui-qunit'], function($, dialog2Widget, layerWidget, browser) {
    'use strict';
    /*jshint quotmark:double */

    module("Dialog2 soy tests");

    test("Dialog2 does creates close button for non-modal", function() {
        var $el = $(aui.dialog.dialog2({
            content: "hello world"
        }));

        ok($el.find(".aui-dialog2-header-close").length, "Expected a close button to be rendered");
    });

    test("Dialog2 does not create close button for modal", function() {
        var $el = $(aui.dialog.dialog2({
            content: "hello world",
            modal: true
        }));

        ok(!$el.find(".aui-dialog2-header-close").length, "Expected no close button to be rendered");
    });


    module("Dialog2 tests", {
        setup: function() {
            sinon.stub(browser, "supportsCalc").returns(true);
        },
        // Creates a mock of a layer object. AJS.layer will return this when passed the given $el
        createLayerMock: function($el) {
            var layerInstance = {
                show: function(){},
                hide: function(){},
                remove: function(){},
                isVisible: function(){},
                on: function(){},
                above: function(){},
                below: function(){}
            };
            var mockedLayer = sinon.mock(layerInstance);
            $el.data("_aui-widget-layer", layerInstance);
            return mockedLayer;
        },
        createContentEl: function() {
            return $(aui.dialog.dialog2({
                content: "Hello world"
            })).appendTo("#qunit-fixture");
        },
        teardown: function() {
            browser.supportsCalc.restore();
            AJS.qunit.removeLayers();
        }
    });

    test("Dialog2 creates a dialog with given content", function() {
        var $el = this.createContentEl();

        var dialog = dialog2Widget($el);

        equal($el[0], dialog.$el[0], "Dialog2 accepts given $el");
    });

    test("Dialog2 wraps layer for show, hide, remove", function() {
        expect(0);
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("show").once();
        layerMock.expects("hide").once();
        layerMock.expects("remove").once();

        dialog.show();
        dialog.hide();
        dialog.remove();

        layerMock.verify();
    });

    test("Dialog2 hide is called on close button click", function() {
        expect(0);
        var $el = this.createContentEl();
        var $close = $("<div></div>").addClass("aui-dialog2-header-close").appendTo($el);
        var dialog = dialog2Widget($el);
        var layerMock = this.createLayerMock(dialog.$el);
        layerMock.expects("hide").once();
        dialog.show();

        $close.click();

        layerMock.verify();
    });

    test("Dialog2 wraps layer events", function() {
        expect(0);
        var $el = this.createContentEl();
        var dialog = dialog2Widget($el);
        var layerMock = this.createLayerMock(dialog.$el);
        var fn = function() {};
        layerMock.expects("on").once().withArgs("show", fn);

        dialog.on("show", fn);

        layerMock.verify();
    });
});
