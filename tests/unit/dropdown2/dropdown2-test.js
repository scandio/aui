define(['dropdown2', 'soy/dropdown2', 'aui-qunit'], function() {
    'use strict';

    function pressKey(keyCode) {
        var e = jQuery.Event('keydown');
        e.keyCode = keyCode;
        $(document).trigger(e);
        this.clock.tick(100);
    }

    module("Dropdown2 Unit Tests", {
        setup: function () {
            AJS.$('#qunit-fixture').html(
                '<ul class="aui-dropdown2-trigger-group">' +
                    '<li>' +
                        '<a id="dropdown2-trigger" aria-owns="dropdown2-test" aria-haspopup="true" class="aui-dropdown2-trigger" href="#dropdown2-test" >Example dropdown</a>' +
                    '</li>' +
                    '<li>' +
                        '<a id="dropdown2-trigger2" aria-owns="dropdown2-test2" aria-haspopup="true" class="aui-dropdown2-trigger" href="#dropdown2-test2" >Example second dropdown</a>' +
                    '</li>' +
                '</ul>' +
                '<div id="dropdown2-test" class="aui-dropdown2"></div>' +
                '<div id="dropdown2-test2" class="aui-dropdown2"></div>' +
                '<div id="hideout"></div>'
            );
            this.$trigger = AJS.$("#dropdown2-trigger");
            this.$trigger2 = AJS.$("#dropdown2-trigger2");
            this.$content = AJS.$("#dropdown2-test");
            this.$content2 = AJS.$("#dropdown2-test2");
            this.$hideout = AJS.$("#hideout");

            this.clock = sinon.useFakeTimers();
        },
        teardown: function () {
            this.clock.restore();

            this.$trigger.remove();
            this.$trigger2.remove();
            this.$content.remove();
            this.$content2.remove();
            this.$hideout.remove();
        },
        simulateTriggerClick: function(num) {
            this['$trigger' + (num || '')].click();
        },
        simulateTriggerHover: function(num) {
            var e = jQuery.Event("mousemove");
            this['$trigger' + (num || '')].trigger(e);
            this.clock.tick(0);
        },
        invokeTrigger: function () {
            this.$trigger.trigger("aui-button-invoke");
            this.clock.tick(100);
        },
        pressKey: function(keyCode) {
            pressKey.call(this, keyCode);
        },
        simulateItemClick: function($item) {
            $item.trigger('click');
        },
        triggerIsActive: function(num) {
            return this['$trigger' + (num || '')].is('.active,.aui-dropdown2-active');
        },
        item: function(itemId, num) {
            return this['$content' + (num || '')].find('#' + itemId);
        },
        addPlainSection: function() {
            AJS.$("#section1").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section1" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="item1">Menu item</a></li>' +
                        '<li><a id="item2">Menu item</a></li>' +
                        '<li><a id="item3">Menu item</a></li>' +
                        '<li><a id="item4">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addPlainSection2: function() {
            AJS.$("#section12").remove();
            AJS.$('#dropdown2-test2').append(
                '<div id="section12" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="item12">Menu item</a></li>' +
                        '<li><a id="item22">Menu item</a></li>' +
                        '<li><a id="item32">Menu item</a></li>' +
                        '<li><a id="item42">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addRadioSection: function() {
            AJS.$("#section2").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section2" class="aui-dropdown2-section">' +
                    '<ul role="radiogroup">' +
                        '<li><a id="radio1-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                        '<li><a id="radio2-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
                        '<li><a id="radio3-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addHiddenSection: function() {
            AJS.$("#section3").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section3" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li class="aui-dropdown2-hidden"><a id="hidden1-unchecked-disabled" class="aui-dropdown2-checkbox interactive disabled" >Menu item</a></li>' +
                        '<li class="aui-dropdown2-hidden"><a id="hidden2-checked" class="aui-dropdown2-checkbox interactive aui-dropdown2-checked">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addInteractiveSection: function() {
            AJS.$("#section4").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section4" class="aui-dropdown2-section">' +
                    '<ul role="radiogroup">' +
                        '<li><a id="iradio1-interactive-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
                        '<li><a id="iradio2-interactive-unchecked" class="aui-dropdown2-radio interactive">Menu item</a></li>' +
                        '<li><a id="iradio3-unchecked" class="aui-dropdown2-radio">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addCheckboxSection: function() {
            AJS.$("#section5").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section5" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="check1-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                        '<li><a id="check2-checked" class="aui-dropdown2-checkbox interactive checked">Menu item</a></li>' +
                        '<li><a id="check3-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        }
    });

    // Testing opening and closing by triggering invoke

    test("Dropdown2 aui-button-invoke opens dropdown", function() {
        this.addPlainSection();
        this.invokeTrigger();

        ok(this.triggerIsActive(), "Dropdown2 is active");
    });

    test("Dropdown2 aui-button-invoke closes dropdown", function() {
        this.addPlainSection();

        this.invokeTrigger();
        this.invokeTrigger();
        
        ok(!this.triggerIsActive(), "Dropdown2 is not active 2");
    });

    test("Dropdown2 does not open when disabled", function() {
        this.addPlainSection();
        this.$trigger.attr('aria-disabled','true');

        this.simulateTriggerClick();

        ok(!this.triggerIsActive(), "Dropdown2 is active");
    });

    // Testing key navigation

    test("Dropdown2 navigated correctly using keys", function() {
        this.addPlainSection();
        this.simulateTriggerClick();

        this.pressKey(AJS.keyCode.DOWN);

        var $i1 = this.item('item1'),
            $i2 = this.item('item2');

        ok(this.triggerIsActive(), "Dropdown2 is active");
        ok(!$i1.is('.aui-dropdown2-active'), "Dropdown2 item 1 is not active");
        ok($i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 is active");
    });

    test("Dropdown2 navigated correctly using keys with hidden items", function() {
        this.addPlainSection();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2'),
            $i3 = this.item('item3'),
            $i4 = this.item('item4');

        $i1.parent().addClass('hidden');
        $i3.parent().addClass('hidden');

        this.simulateTriggerClick();
        this.pressKey(AJS.keyCode.DOWN);

        ok(!$i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 not active");
        ok($i4.is('.aui-dropdown2-active'), "Dropdown2 item 4 is active");
    });

    test("Dropdown2 navigated correctly using keys with hidden items that were added after opened", function() {
        this.addPlainSection();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2'),
            $i3 = this.item('item3'),
            $i4 = this.item('item4');

        $i1.parent().addClass('hidden');

        this.simulateTriggerClick();
        $i3.attr('aria-disabled','true').addClass('disabled').parent().addClass('hidden');
        this.pressKey(AJS.keyCode.DOWN);

        ok(!$i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 was skipped");
        ok($i4.is('.aui-dropdown2-active'), "Dropdown2 item 4 was skipped");
    });

    // --- Testing Events ---

    // Show Events

    test("Dropdown2 fires aui-dropdown2-show when it is shown by click", function() {
        this.addPlainSection();

        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-show", spy);

        this.simulateTriggerClick();

        ok(spy.calledOnce, "Expected event listener called on show");
    });

    test("Dropdown2 fires aui-dropdown2-show when it is shown by invoke", function() {
        this.addPlainSection();

        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-show", spy);

        this.invokeTrigger();

        ok(spy.calledOnce, "Expected event listener called on show");
    });

    // Hide Events

    test("Dropdown2 fires aui-dropdown2-hide when it is hidden by click", function() {
        this.addPlainSection();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-hide", spy);

        this.clock.tick(1000);
        this.simulateTriggerClick();
        this.clock.tick(1000);
        this.simulateTriggerClick();
        this.clock.tick(1000);

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-hide when it is hidden by invoke", function() {
        this.addPlainSection();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-hide", spy);

        this.simulateTriggerClick();
        this.invokeTrigger();

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    // Check and uncheck Events on checkboxes and radios

    test("Dropdown2 fires aui-dropdown2-item-check on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked'),
            $c3 = this.item('check3-unchecked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);
        this.simulateItemClick($c3);

        ok(spy.calledTwice, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-uncheck on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-uncheck", spy);

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked'),
            $c3 = this.item('check3-unchecked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);
        this.simulateItemClick($c3);

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-check on radios", function() {
        this.addRadioSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked'),
            $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r2);
        this.simulateItemClick($r1);
        this.simulateItemClick($r3);

        ok(spy.calledTwice, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-uncheck on radios", function() {
        this.addRadioSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked'),
            $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r1);
        this.simulateItemClick($r2);
        this.simulateItemClick($r3);

        ok(spy.calledThrice, "Expected event listener called on hide");
    });

    // Hide location

    test("Dropdown2 returned to original location if data-dropdown2-hide-location is not specified", function() {
        this.addPlainSection();
        var $hideParent = this.$content.parent()[0];

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.$content.parent()[0] === $hideParent, "Dropdown placed in original location");
        ok(this.$hideout.find(this.$content).length === 0, "Hideout does not contain dropdown");
    });

    test("Dropdown2 specifying the data-dropdown2-hide-location works properly when dropdown is hidden", function() {
        this.addPlainSection();
        this.$trigger.attr('data-dropdown2-hide-location', 'hideout');
        var $originalParent = this.$content.parent()[0];

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.$content.parent()[0] !== $originalParent, "Dropdown not placed in original location");
        ok(this.$hideout.find(this.$content).length === 1, "Hideout contains dropdown");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on click correctly one dropdown", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.triggerIsActive(), "Dropdown2 first is active");
        ok(!this.triggerIsActive(2), "Dropdown2 second is not active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on click correctly multiple dropdowns", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick(2);
        this.clock.tick(100);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on hover after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerHover(2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group doesnt open on over when one not clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerHover(2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(!this.triggerIsActive(2), "Dropdown2 second is not active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group key navigation up down after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        this.pressKey(AJS.keyCode.DOWN);

        var $i1 = this.item('item1'),
            $i2 = this.item('item2');

        ok(this.triggerIsActive(), "Dropdown2 first is not active");
        ok(!$i1.is('.aui-dropdown2-active'), "Dropdown2 item 1 is not active");
        ok($i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group key navigation left right after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        this.pressKey(AJS.keyCode.RIGHT);
        this.clock.tick(100);
        this.pressKey(AJS.keyCode.DOWN);
        this.clock.tick(100);

        var $i12 = this.item('item12',2),
            $i22 = this.item('item22',2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
        ok(!$i12.is('.aui-dropdown2-active'), "Dropdown2 second item 1 is not active");
        ok($i22.is('.aui-dropdown2-active'), "Dropdown2 second item 2 is active");
    });

    test("Dropdown2 adds aui-dropdown2-active and active to trigger on click", function() {
        this.addPlainSection();
        this.simulateTriggerClick();

        ok(this.$trigger.is('.aui-dropdown2-active'), "Dropdown2 has aui-dropdown2-active on click");
        ok(this.$trigger.is('.active'), "Dropdown2 has active on click");
    });

    test("Dropdown2 adds aui-dropdown2-checked on click checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();

        var $c1 = this.item('check1-unchecked');

        this.simulateItemClick($c1);

        ok($c1.is('.checked.aui-dropdown2-checked'), "Dropdown2 checkbox 1 is checked");
    });

    test("Dropdown2 toggles checked to unchecked on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);

        ok($c1.is('.checked.aui-dropdown2-checked'), "Dropdown2 checkbox 1 is checked");
        ok(!$c2.is('.checked,.aui-dropdown2-checked'), "Dropdown2 checkbox 2 is unchecked");
    });

    test("Dropdown2 adds aui-dropdown2-checked on click radio buttons", function() {
        this.addRadioSection();
        this.simulateTriggerClick();

        var $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r3);

        ok($r3.is('.checked.aui-dropdown2-checked'), "Dropdown2 radio 3 is checked");
    });

    test("Dropdown2 toggles checked to unchecked on radio buttons", function() {
        this.addRadioSection();
        this.simulateTriggerClick();

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked');

        this.simulateItemClick($r1);

        ok($r1.is('.checked.aui-dropdown2-checked'), "Dropdown2 radio 1 is checked");
        ok(!$r2.is('.checked,.aui-dropdown2-checked'), "Dropdown2 radio 2 is unchecked");
    });

    test("Dropdown2 adds aui-dropdown2-disabled to hidden item - li.hidden > a", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h2 = this.item('hidden2-checked');

        ok($h2.is('.disabled.aui-dropdown2-disabled'), "Dropdown2 hidden item 2 is aui-dropdown2-disabled");
    });

    test("Dropdown2 adds aui-dropdown2-disabled to hidden and disabled item - li.hidden > a", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h1 = this.item('hidden1-unchecked-disabled');

        ok($h1.is('.disabled.aui-dropdown2-disabled'), "Dropdown2 hidden item 1 is aui-dropdown2-disabled");
    });

    test("Dropdown2 cannot click aui-dropdown2-disabled", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h1 = this.item('hidden1-unchecked-disabled'),
            $h2 = this.item('hidden2-checked');

        this.simulateItemClick($h1);
        this.simulateItemClick($h2);

        ok(!$h1.is('.checked.aui-dropdown2-checked'), "Dropdown2 hidden item 1 not checked when clicked");
        ok($h2.is('.aui-dropdown2-checked'), "Dropdown2 hidden item 2 not unchecked when clicked");
    });

    test("Dropdown2 aui-dropdown2-interactive doesnt hide on click", function() {
        this.addInteractiveSection();
        this.simulateTriggerClick();

        var $ir1 = this.item('iradio1-interactive-checked'),
            $ir2 = this.item('iradio2-interactive-unchecked');

        this.simulateItemClick($ir1);
        this.simulateItemClick($ir2);
        this.clock.tick(100);

        ok(this.triggerIsActive(), "Dropdown2 has aui-dropdown2-active on click");
    });

    test("Dropdown2 not aui-dropdown2-interactive hides on click", function() {
        this.addInteractiveSection();
        this.simulateTriggerClick();

        var $ir3 = this.item('iradio3-unchecked');

        this.simulateItemClick($ir3);
        this.clock.tick(100);

        ok(!this.triggerIsActive(), "Dropdown2 has aui-dropdown2-active on click");
    });

    module("Dropdown2 submenu Unit Tests", {
        setup: function () {
            AJS.$('body').append(
                '<ul id="dd2-ul" class="aui-dropdown2-trigger-group">' +
                    '<li>' +
                        '<a id="dd2-trigger" href="#dd2-menu" aria-owns="dd2-menu-1" aria-haspopup="true" class="aui-dropdown2-trigger aui-style-default" aria-controls="dd2-menu">Open dropdown</a>' +
                        '<div id="dd2-menu-1" class="aui-dropdown2 aui-style-default" aria-hidden="true">' +
                            '<ul class="aui-list-truncate">' +
                                '<li>' +
                                    '<a id="dd2-menu-1-child-1">Dummy item 1</a>' +
                                    '<a id="dd2-menu-1-child-2" href="#dd2-submenu-1" aria-owns="dd2-menu-2" aria-haspopup="true" class="interactive aui-dropdown2-sub-trigger aui-style-default" aria-controls="dd2-menu-2">Open submenu level 1</a>' +
                                    '<div id="dd2-menu-2" class="aui-dropdown2 aui-style-default aui-dropdown2-sub-menu" aria-hidden="true">' +
                                        '<ul class="aui-list-truncate">' +
                                            '<li>' +
                                                '<a id="dd2-menu-2-child-1" class="">Dummy item 2</a>' +
                                                '<a id="dd2-menu-2-child-2" href="#dd2-menu-3" aria-owns="dd2-menu-3" aria-haspopup="true" class="interactive aui-dropdown2-sub-trigger aui-style-default" aria-controls="dd2-menu-3">Open submenu level 2</a>' +
                                                '<div id="dd2-menu-3" class="aui-dropdown2 aui-style-default aui-dropdown2-sub-menu" aria-hidden="true">' +
                                                    '<ul class="aui-list-truncate">' +
                                                        '<li>' +
                                                            '<a>Final level</a>' +
                                                        '</li>' +
                                                    '</ul>' +
                                                '</div>' +
                                            '</li>' +
                                        '</ul>' +
                                    '</div>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</li>' +
                '</ul>'
            );

            this.clock = sinon.useFakeTimers();
        },
        teardown: function(){
            AJS.$('#dd2-ul').remove();
            AJS.$('#dd2-menu-1').remove();
            AJS.$('#dd2-menu-2').remove();
            AJS.$('#dd2-menu-3').remove();
            this.clock.restore();
        },
        getMenuItem: function(level, child) {
            var selector = '#dd2-menu-' + level + '-child-' + child;
            return AJS.$(selector);
        },
        getFirstMenuTrigger: function() {
            return AJS.$('#dd2-trigger');
        },
        getSecondMenuTrigger: function() {
            return this.getMenuItem(1, 2);
        },
        getThirdMenuTrigger: function() {
            return this.getMenuItem(2, 2);
        },
        pressKey: function(keyCode) {
            pressKey.call(this, keyCode);
        },
        invokeTrigger: function($el) {
            $el.trigger('aui-button-invoke');
            this.clock.tick(100);
        },
        hoverOver: function($el) {
            $el.mousemove();
            this.clock.tick(100);
        },
        clickOnDocument: function() {
            AJS.$(document).click();
            this.clock.tick(100);
        },
        openAllMenus: function() {
            var $firstMenuTrigger = this.getFirstMenuTrigger();
            var $secondMenuTrigger = this.getSecondMenuTrigger();
            var $thirdMenuTrigger = this.getThirdMenuTrigger();

            this.invokeTrigger($firstMenuTrigger);
            this.invokeTrigger($secondMenuTrigger);
            this.invokeTrigger($thirdMenuTrigger);

            this.clock.tick(100);
        },
        countOpenDropdowns: function() {
            return AJS.$('.aui-dropdown2[aria-hidden=false]').length;
        }
    });

    test('Dropdown2 with submenus opens first menu', function() {
        var $trigger = this.getFirstMenuTrigger();
        $trigger.click();
        ok($trigger.hasClass('active'), 'Clicking on a trigger with a submenu, the trigger becomes active');
    });

    test('Dropdown2 submenus can be opened', function() {
        var $firstMenuTrigger = this.getFirstMenuTrigger();
        var $secondMenuTrigger = this.getSecondMenuTrigger();
        var $secondMenuFirstChild = this.getMenuItem(2, 1);

        this.invokeTrigger($firstMenuTrigger);
        this.invokeTrigger($secondMenuTrigger);

        ok($secondMenuTrigger.hasClass('active'), 'The first submenu trigger is active');
        ok($secondMenuFirstChild.hasClass('active'), 'The first submenu trigger\'s first child is active');
    });

    test('Escape closes one submenu at a time', function() {
        this.openAllMenus();

        strictEqual(this.countOpenDropdowns(), 3, 'After opening two submenus there are 3 dropdowns visible total');
        this.pressKey(AJS.keyCode.ESCAPE);
        strictEqual(this.countOpenDropdowns(), 2, 'After opening two submenus and pressing escape once there are 2 dropdowns visible total');
        this.pressKey(AJS.keyCode.ESCAPE);
        strictEqual(this.countOpenDropdowns(), 1, 'After opening two submenus and pressing escape twice there are 1 dropdowns visible total');
        this.pressKey(AJS.keyCode.ESCAPE);
        strictEqual(this.countOpenDropdowns(), 0, 'After opening two submenus and pressing escape three times there are no visible dropdowns');
    });

    test('Clicking on the document closes all submenus', function() {
        this.openAllMenus();
        strictEqual(this.countOpenDropdowns(), 3, 'Before clicking on the document there are 3 open dropdowns');
        this.clickOnDocument();
        strictEqual(this.countOpenDropdowns(), 0, 'After clicking on the document there are no open dropdowns');
    });

    test('Expanding submenus via the keyboard works', function() {
        var $trigger = this.getFirstMenuTrigger();
        $trigger.click();
        this.pressKey(AJS.keyCode.DOWN);
        this.pressKey(AJS.keyCode.RIGHT);
        strictEqual(this.countOpenDropdowns(), 2, 'Two menus should be visible after opening one submenu');
        this.pressKey(AJS.keyCode.DOWN);
        this.pressKey(AJS.keyCode.RIGHT);
        strictEqual(this.countOpenDropdowns(), 3, 'Three menus should be visible after opening two submenus');
    });

    test('Pressing right from a non trigger does not open a dropdown', function() {
        var $trigger = this.getFirstMenuTrigger();
        $trigger.click();
        this.pressKey(AJS.keyCode.RIGHT);
        strictEqual(this.countOpenDropdowns(), 1, 'One menu should be visible after pressing right on a non trigger');
    });

    test('Pressing left from any position in a dropdown closes the submenu', function() {
        this.openAllMenus();
        this.pressKey(AJS.keyCode.LEFT);

        strictEqual(this.countOpenDropdowns(), 2, 'One menu has closed after pressing left from a trigger');

        this.pressKey(AJS.keyCode.UP);
        this.pressKey(AJS.keyCode.LEFT);

        strictEqual(this.countOpenDropdowns(), 1, 'One menu has closed after pressing left from a non trigger');
    });

    test('The last menu cannot be closed by the arrow keys', function() {
        var $trigger = this.getFirstMenuTrigger();
        $trigger.click();

        this.pressKey(AJS.keyCode.UP);
        this.pressKey(AJS.keyCode.DOWN);
        this.pressKey(AJS.keyCode.LEFT);
        this.pressKey(AJS.keyCode.RIGHT);

        ok(this.countOpenDropdowns() >= 1, 'The last menu cannot be closed by the arrow keys');
    });

    test('Dropdown submenus can be opened by hovering', function () {
        var $trigger = this.getFirstMenuTrigger();
        $trigger.click();

        this.hoverOver(this.getMenuItem(1,2));
        this.hoverOver(this.getMenuItem(2,2));

        strictEqual(this.countOpenDropdowns(), 3, 'After hovering on multiple triggers, all menus are open');

        this.hoverOver(this.getMenuItem(1,1));

        strictEqual(this.countOpenDropdowns(), 1, 'After hovering on the first menu again, other submenus close');


    });

});