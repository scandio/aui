(function($) {
    
    var $overflowEl;

    /**
     *
     * Dims the screen using a blanket div
     * @param useShim deprecated, it is calculated by dim() now
     */
    AJS.dim = function (useShim, zIndex) {

        if  (!$overflowEl) {
            $overflowEl = $(document.body);
        }

        if (useShim === true) {
            useShimDeprecationLogger();
        }

        if (!AJS.dim.$dim) {
            AJS.dim.$dim = AJS("div").addClass("aui-blanket");
            AJS.dim.$dim.attr('tabindex', '0'); //required, or the last element's focusout event will go to the browser
            if (zIndex) {
                AJS.dim.$dim.css({zIndex: zIndex});
            }

            $("body").append(AJS.dim.$dim);
            
            AJS.dim.cachedOverflow = $overflowEl.css("overflow");
            $overflowEl.css("overflow", "hidden");
        }

        return AJS.dim.$dim;
    };

    /**
     * Removes semitransparent DIV
     * @see AJS.dim
     */
    AJS.undim = function() {
        if (AJS.dim.$dim) {
            AJS.dim.$dim.remove();
            AJS.dim.$dim = null;

            $overflowEl && $overflowEl.css("overflow",  AJS.dim.cachedOverflow);

            // Safari bug workaround
            if ($.browser.safari) {
                var top = $(window).scrollTop();
                $(window).scrollTop(10 + 5 * (top == 10)).scrollTop(top);
            }
        }
    };

    var useShimDeprecationLogger = AJS.deprecate.getMessageLogger('useShim', {
        extraInfo: 'useShim has no alternative as it is now calculated by dim().'
    });

}(AJS.$));