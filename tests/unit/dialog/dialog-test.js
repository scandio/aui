/*jshint quotmark:double, eqeqeq: false */
require(["dialog"], function() {
    module("Dialog", {
        teardown: function() {
            AJS.$(".aui-popup").remove();
        }
    });

    test("test for Popup API creation", function() {
        var testPopup = AJS.popup({height:500, width: 500, id:"test-popup"});

        ok(typeof testPopup == "object", "testPopup was created successfully!");
        ok(typeof testPopup.changeSize == "function", "testPopup has the change size function!");
        ok(typeof testPopup.disable == "function", "testPopup has the diable function!");
        ok(typeof testPopup.enable == "function", "testPopup has the enable function!");
        ok(typeof testPopup.hide == "function", "testPopup has the hide function!");
        ok(typeof testPopup.remove == "function", "testPopup has the remove function!");
        ok(typeof testPopup.show == "function", "testPopup has the show function!");
    });

    test("test for Popup div creation", function() {
        AJS.popup({height:500, width: 500, id:"test-popup"});
        ok(AJS.$("#test-popup").size()!=0, "test-popup div exists!");
    });

    test("test for Popup correct size creation", function() {
        AJS.popup({height:500, width: 500, id:"test-popup"});
        AJS.popup({height:123, width: 234, id:"test-popup2"});
        ok(AJS.$("#test-popup").height()==500, "test-popup is " + AJS.$("#test-popup").height() + "px in height, expected 500px");
        ok(AJS.$("#test-popup").width()==500, "test-popup is " + AJS.$("#test-popup").width() + "px in width, expected 500px");
        ok(AJS.$("#test-popup2").height()==123, "test-popup2 is " + AJS.$("#test-popup2").height() + "px in height, expected 123px");
        ok(AJS.$("#test-popup2").width()==234, "test-popup2 is " + AJS.$("#test-popup2").width() + "px in width, expected 234px");
    });

    test("test for Dialog API creation", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog == "object", "testPopup was created successfully!");
        ok(typeof testDialog.addHeader == "function", "testDialog has the addHeader function!");
        ok(typeof testDialog.addButton == "function", "testDialog has the addButton function!");
        ok(typeof testDialog.addSubmit == "function", "testDialog has the addLink function!");
        ok(typeof testDialog.addLink == "function", "testDialog has the addSubmit function!");
        ok(typeof testDialog.addButtonPanel == "function", "testDialog has the addButtonPanel function!");
        ok(typeof testDialog.addPanel == "function", "testDialog has the addPanel function!");
        ok(typeof testDialog.addPage == "function", "testDialog has the addPage function!");
        ok(typeof testDialog.nextPage == "function", "testDialog has the nextPage function!");
        ok(typeof testDialog.prevPage == "function", "testDialog has the prevPage function!");
        ok(typeof testDialog.gotoPage == "function", "testDialog has the gotoPage function!");
        ok(typeof testDialog.getPanel == "function", "testDialog has the getPanel function!");
        ok(typeof testDialog.getPage == "function", "testDialog has the getPage function!");
        ok(typeof testDialog.getCurrentPanel == "function", "testDialog has the getCurrentPanel function!");
        ok(typeof testDialog.gotoPanel == "function", "testDialog has the gotoPanel function!");
        ok(typeof testDialog.show == "function", "testDialog has the show function!");
        ok(typeof testDialog.hide == "function", "testDialog has the hide function!");
        ok(typeof testDialog.remove == "function", "testDialog has the remove function!");
        ok(typeof testDialog.disable == "function", "testDialog has the disable function!");
        ok(typeof testDialog.enable == "function", "testDialog has the enable function!");
        ok(typeof testDialog.get == "function", "testDialog has the get function!");
        ok(typeof testDialog.updateHeight == "function", "testDialog has the updateHeight function!");
    });

    test("test for Dialog.addHeader", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addHeader("HEADER", "header") == "object", "a dialog header was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-title").size()!=0, "dialog-title div exists as expected!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-title").hasClass("header"), "dialog-title div has the 'header' class as expected!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-title").text()=="HEADER", "dialog-title has the text 'HEADER' as expected!");
    });

    test("test for Dialog.addHeader html escaping", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        testDialog.addHeader("<u>foo & bar</u>");
        ok(AJS.$("#test-dialog .dialog-components .dialog-title").html()=="&lt;u&gt;foo &amp; bar&lt;/u&gt;", "dialog-title has the heading HTML escaped as expected!");
    });

    test("test for Dialog.addButton", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addButton("BUTTON", function(){}, "button") == "object", "a dialog button was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel button.button-panel-button").size()!=0, "test-dialog has a button-panel-button button element as expected");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel button.button-panel-button").text()=="BUTTON", "test-dialog's button-panel-button button element has text 'BUTTON' as expected");
    });

    test("test for Dialog.addLink", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addLink("LINK", function(){}, "link") == "object", "a dialog link was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel a.button-panel-link").size()!=0, "test-dialog has a button-panel-link a element as expected");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel a.button-panel-link").text()=="LINK", "test-dialog's button-panel-link a element has text 'LINK' as expected");
    });

    test("test for Dialog.addSubmit", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addSubmit("SUBMIT", function(){}) == "object", "a dialog submit button was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel button.button-panel-submit-button").size()!=0, "test-dialog has a button-panel-submit-button button element as expected");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel button.button-panel-submit-button").text()=="SUBMIT", "test-dialog's button-panel-button button element has text 'SUBMIT' as expected");
    });

    test("test for Dialog.addCancel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addCancel("CANCEL", function(){}) == "object", "a dialog cancel link was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel a.button-panel-cancel-link").size()!=0, "test-dialog has a button-panel-cancel-link a element as expected");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel a.button-panel-cancel-link").text()=="CANCEL", "test-dialog's button-panel-cancel-link a element has text 'CANCEL' as expected");
    });

    test("test for Dialog.addButtonPanel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addButtonPanel() == "object", "a dialog button panel was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components .dialog-button-panel").size()!=0, "test-dialog has a button-panel element as expected");
    });

    test("test for Dialog.addPanel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addPanel("panel", "some text", "panel-body") == "object", "a dialog panel was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components div.dialog-page-body div.dialog-panel-body").size()!=0, "test-dialog has a dialog-panel-body element as expected");
        ok(AJS.$("#test-dialog .dialog-components ul.dialog-page-menu li.page-menu-item button.item-button").text()=="panel", "test-dialog has a page-menu-item li with text 'panel' as expected");

    });

    test("test for Dialog.addPanel x 2", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addPanel("panel", "some text", "panel-body") == "object", "a dialog panel was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components div.dialog-page-body div.dialog-panel-body:nth-child(1)").size()!=0, "test-dialog has a dialog-panel-body element as expected");
        ok(typeof testDialog.addPanel("panel", "some text", "panel-body") == "object", "a second dialog panel was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components div.dialog-page-body div.dialog-panel-body:nth-child(2)").size()!=0, "test-dialog has a dialog-panel-body element as expected");
    });

    test("test for Dialog.addPanel with id", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.addPanel("panel", "some text", "panel-body", "panel-1") == "object", "a dialog panel was added successfully!");
        ok(AJS.$("#test-dialog .dialog-components div.dialog-page-body div.dialog-panel-body:nth-child(1)").size()!=0, "test-dialog has a dialog-panel-body element as expected");
        ok(AJS.$("#test-dialog .dialog-components ul.dialog-page-menu li.page-menu-item button.item-button").attr("id")=="panel-1", "test-dialog has a page-menu-item li with an id of 'panel-1' as expected");
    });

    test("test for Dialog.addPage", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        var newPage = testDialog.addPage("page");
        ok(typeof newPage == "object", "a dialog page was added successfully!");
        ok(!AJS.$(newPage.page[1].body).is(":visible"), "the new page that was added is hidden.");
        ok(testDialog.curpage == 1, "The current page has been changed to the last one added");
    });

    test("test for Dialog.nextPage", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        testDialog.addPage();
        testDialog.addPage();
        testDialog.addPage();
        ok(typeof testDialog.nextPage() == "object", "returned the next page successfully!");
        ok(testDialog.curpage == 0, "The current page has been changed to the first one");
        testDialog.nextPage();
        ok(testDialog.curpage == 1, "The current page has been changed to the next one");
        testDialog.nextPage();
        ok(testDialog.curpage == 2, "The current page has been changed to the next one");
    });

    test("test for Dialog.prevPage", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.prevPage() == "object", "returned the previous page successfully!");
        testDialog.addPage();
        testDialog.addPage();
        testDialog.addPage();
        ok(typeof testDialog.nextPage() == "object", "returned the next page successfully!");
        ok(testDialog.curpage == 0, "The current page has been changed to the first one");
        testDialog.nextPage();
        ok(testDialog.curpage == 1, "The current page has been changed to the next one");
        testDialog.nextPage();
        ok(testDialog.curpage == 2, "The current page has been changed to the next one");
    });

    test("test for Dialog.gotoPage", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.gotoPage(0) == "object", "went to the specified page successfully!");
    });

    test("test for Dialog.getPanel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        testDialog.addPanel("panel", "some text", "panel-body");
        ok(typeof testDialog.getPanel(0, 0) == "object", "getPanel() returned the panel successfully!");
    });

    test("test for Dialog.getPage", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        ok(typeof testDialog.getPage(0) == "object", "went to the specified page successfully!");
    });

    test("test for Dialog.getCurrentPanel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        testDialog.addPanel("panel", "some text", "panel-body");
        ok(typeof testDialog.getCurrentPanel() == "object", "returned the current panel successfully!");
    });

    test("test for Dialog.updateHeight", function() {
        var testDialog = new AJS.Dialog({ width: 500, height: 100, id:"test-dialog" });
        testDialog.addHeader("Test Dialog");
        testDialog.addPanel("", "<div>Foobar</div>");
        testDialog.addButtonPanel();
        testDialog.addButton("Test Button");
        testDialog.addCancel("Cancel", function() {});
        testDialog.show();
        var h0 = document.getElementById("test-dialog").offsetHeight;
        testDialog.getCurrentPanel().body.html('<div style="height:123px"></div>');
        testDialog.updateHeight();
        var h1 = document.getElementById("test-dialog").offsetHeight;
        ok(h1 > h0,  "dialog.updateHeight() increases dialog element height");
        ok(h1 > 123, "dialog.updateHeight() resizes dialog to fit content");
    });

    test("test for dialog .addPanel", function() {
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog"});
        testDialog.addPanel("panel", "some text", "panel-body");
        ok(typeof testDialog.getCurrentPanel() == "object", "returned the current panel successfully!");
    });

    test("test for dialog not binding keyboard shortcuts more than once", function() {
        var count = 0;
        var listener = function() {
            count++;
        };
        var testDialog = new AJS.Dialog({height:500, width: 500, id:"test-dialog", keypressListener: listener});
        testDialog.addPanel("panel", "some text", "panel-body");
        testDialog.show();
        $(document).trigger("keydown");
        equal(count, 1, "key handler was called once.");
        testDialog.updateHeight();
        $(document).trigger("keydown");
        equal(count, 2, "key handler was called a second (only) time.");
    });
});