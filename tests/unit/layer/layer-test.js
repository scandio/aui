define(['layer', 'layer-manager', 'dialog2', 'aui-qunit', 'aui/internal/skate', 'aui/internal/tether'], function(layerWidget, LayerManager, dialog2Widget) {
    'use strict';


    function onEvent(layer, event, fn) {
        layer.addEventListener('aui-layer-' + event, fn);
    }

    function offEvent(layer, event, fn) {
        layer.removeEventListener('aui-layer-' + event, fn);
    }


    var modulePrototype = {
        setup: function() {
            this.dimSpy = sinon.spy(AJS, 'dim');
            this.undimSpy = sinon.spy(AJS, 'undim');
            LayerManager.global = new LayerManager();
            this.focusManagerGlobalEnterSpy = sinon.spy(AJS.FocusManager.global, 'enter');
            this.focusManagerGlobalExitSpy = sinon.spy(AJS.FocusManager.global, 'exit');
        },
        teardown: function() {
            this.dimSpy.restore();
            this.undimSpy.restore();
            this.focusManagerGlobalEnterSpy.restore();
            this.focusManagerGlobalExitSpy.restore();
            AJS.qunit.removeLayers();
        },
        createLayer: function(options) {
            options = options || {};
            var $el = AJS.$('<div></div>').attr('aria-hidden', 'true').appendTo('#qunit-fixture');
            if (options.blanketed) {
                $el.attr('data-aui-blanketed', true);
            }

            if (options.modal) {
                $el.attr('data-aui-modal', true);
            }

            if (options.persistent) {
                $el.attr('data-aui-persistent', true);
            }

            //layer needs some height so that layer :visible selector works properly
            $el.height(100);
            return $el;
        }
    };


    module('Layer - initialisation', modulePrototype);

    test('Layer creates a div by default', function() {
        var layer = layerWidget();

        ok(layer.$el.is('div'), 'Expected a default element to be created');
        ok(layer.$el.is('.aui-layer'), 'Expected an aui-layer class to be present');
        equal(layer.$el.attr('aria-hidden'), 'true', 'Expected a default layer to be created');
    });

    test('Layer works with passed jQuery element', function() {
        var $el = AJS.$('<div></div>').appendTo('#qunit-fixture');
        var layer = layerWidget($el);

        equal(layer.$el[0], $el[0], 'Expected that the given element was bound');
        ok(layer.$el.is('.aui-layer'), 'Expected an aui-layer class to be present');
    });

    test('Layer works with passed HTML element', function() {
        var element = document.createElement('div');
        AJS.$('#qunit-fixture').append(element);
        var layer = layerWidget(element);

        equal(layer.$el[0], element, 'Expected that the given element was bound');
        ok(layer.$el.is('.aui-layer'), 'Expected an aui-layer class to be present');
    });

    test('Layer respects hidden state of passed element', function() {
        var $el = AJS.$('<div></div>').attr('aria-hidden','false').appendTo('#qunit-fixture');
        var layer = layerWidget($el);

        equal(layer.$el[0], $el[0], 'Expected that the given element was bound');
        equal(layer.$el.attr('aria-hidden'), 'false', 'Existing aria-hidden value preserved');
    });

    test('Layer works with passed selector', function() {
        var $el = this.createLayer().attr('id', 'my-layer');
        var layer = layerWidget('#my-layer');

        equal(layer.$el[0], $el[0], 'Expected that the given element was bound');
    });

    test('Layer works with DOM node', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el[0]);

        equal(layer.$el[0], $el[0], 'Expected that the given element was bound');
    });

    test('Layer LayerManager barfs if the same element is layered twice', function() {
        var $el = this.createLayer();
        layerWidget($el).show();

        throws(function() {
            LayerManager.global.push($el);
        }, 'Should throw an exception');
    });


    module('Layer - showing, hiding and removing', modulePrototype);

    test('Layer shows element', function() {
        var $el = this.createLayer();

        layerWidget($el).show();

        equal($el.attr('aria-hidden'), 'false', 'Expected layer is not hidden');
    });

    test('Layer shows appends to body', function() {
        var $el = this.createLayer();

        layerWidget($el).show();

        ok($el.parent().is('body'), 'Layer is appended to body');
    });

    test('Layer shows sets z index', function() {
        var $el = this.createLayer();

        layerWidget($el).show();

        equal($el.css('z-index'), 3000, 'Layer z-index is initialised correctly');
    });

    test('Layer shows requests focus when blanketed', function() {
        var $el = this.createLayer({blanketed: true});

        layerWidget($el).show();

        equal(AJS.FocusManager.global.enter.callCount, 1, 'Focus is requested');
        var $focusEl = AJS.FocusManager.global.enter.args[0][0];
        equal($focusEl[0], $el[0], 'Focus element is passed');
    });

    test('Layer hide hides element', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        layer.show();

        layer.hide();

        equal($el.attr('aria-hidden'), 'true', 'Expected layer is hidden');
    });

    test('Layer hide forces blur when blanketed', function() {
        var $el = this.createLayer({blanketed: true});
        var layer = layerWidget($el);
        layer.show();

        layer.hide();

        equal(AJS.FocusManager.global.exit.callCount, 1, 'Blur is requested');
        var $focusEl = AJS.FocusManager.global.exit.args[0][0];
        equal($focusEl[0], $el[0], 'Focus element is passed');
    });

    test('Layer hide blurs focus on multiple elements when blanketed', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({blanketed: true});
        layerWidget($el1).show();
        layerWidget($el2).show();

        layerWidget($el1).hide();

        equal(AJS.FocusManager.global.exit.callCount, 2, 'Focus manager exit should have been called twice');
        var $focusEl2 = AJS.FocusManager.global.exit.args[0][0];
        equal($focusEl2[0], $el2[0], 'Focus manager exit was called with correct element');
        var $focusEl1 = AJS.FocusManager.global.exit.args[1][0];
        equal($focusEl1[0], $el1[0], 'Focus manager exit was called with correct element');
    });

    test('Layer hide hides multiple elements', function() {
        var $el1 = this.createLayer({modal: true});
        var $el2 = this.createLayer();
        layerWidget($el1).show();
        layerWidget($el2).show();

        layerWidget($el1).hide();

        equal($el1.attr('aria-hidden'), 'true', 'Expected layer is hidden');
        equal($el2.attr('aria-hidden'), 'true', 'Expected layer is hidden');
    });

    test('Layer hide restores original z-index', function() {
        var $el = this.createLayer();
        $el.css('z-index', 1234);
        var layer = layerWidget($el);
        layer.show();

        layer.hide();

        equal($el.css('z-index'), 1234, 'Original z-index is restored');
    });

    test('Layer remove removes element', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        layer.show();

        layer.remove();

        equal($el.attr('aria-hidden'), 'true', 'Expected layer is hidden');
        ok(!$el.parent().length, 'Layer el should have no parent');
    });

    test('Layer increases z-index for multiple layers', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer();

        layerWidget($el1).show();
        layerWidget($el2).show();

        ok($el1.css('z-index') < $el2.css('z-index'), 'z-index should be set higher than first layer');
    });

    test('Layer hide respects persistent layers above it', function() {
        var $el1 = this.createLayer({persistent: true});
        var $el2 = this.createLayer({persistent: true});
        var $el3 = this.createLayer();

        layerWidget($el1).show();
        layerWidget($el2).show();
        layerWidget($el3).show();
        layerWidget($el1).hide();

        strictEqual($el1.attr('aria-hidden'), 'true', 'Expected first element hidden');
        strictEqual($el2.attr('aria-hidden'), 'false', 'Expected second element visible');
        strictEqual($el3.attr('aria-hidden'), 'true', 'Expected third element hidden');
    });


    module('Layer - changing size', modulePrototype);

    test('Layer changeSize to content height', function() {
        var $el = this.createLayer($el);

        AJS.$('<div></div>').css('height', 99999).appendTo($el);

        var layer = layerWidget($el);

        layer.show();

        layer.changeSize(666, 'content');

        equal($el.width(), 666, 'Width should be set');

        // test based on the height of the inner contents
        ok($el.attr('style').indexOf('height') < 0, 'Height should be empty');
    });

    test('Layer changeSize to fixed height', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        layer.show();

        layer.changeSize(666, 999);

        equal($el.width(), 666, 'Width should be set');
        equal($el.height(), 999, 'Height should be set');
    });


    module('Layer - blanketing', modulePrototype);

    test('Layer does not create blanket by default', function() {
        var $el = this.createLayer();

        layerWidget($el).show();

        ok(!this.dimSpy.called, 'Dim should not be called');
    });

    test('Layer creates blanket with specific z-index when specified', function() {
        var $el = this.createLayer({blanketed: true});
        layerWidget($el).show();

        ok(this.dimSpy.calledOnce, 'Dim should have been called');
        equal(this.dimSpy.args[0][1], 2980, 'Dim was called with specific z-index');
    });

    test('Layer does not create blanket when data-aui-blanketed="false"', function() {
        var $el = this.createLayer().attr('data-aui-blanketed', 'false');
        layerWidget($el).show();

        ok(!this.dimSpy.called, 'Dim should not be called');
    });

    test('Layer creates blanket with specific z-index for multiple layers', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({blanketed: true});

        layerWidget($el1).show();
        layerWidget($el2).show();

        equal(this.dimSpy.args[1][1], 3080, 'Dim was called with specific z-index');
    });


    module('Layer - popping', modulePrototype);

    test('Layer pops dim and re-adds for stacked layers when specified', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({blanketed: true});

        layerWidget($el1).show();
        layerWidget($el2).show();

        equal(this.dimSpy.callCount, 2, 'Dim should have been called twice');
        equal(this.undimSpy.callCount, 1, 'Undim should have been called once');
    });

    test('Layer pops dim and re-adds for stacked layers when specified when some have no blanket', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer();
        var $el3 = this.createLayer();
        var $el4 = this.createLayer({blanketed: true});

        layerWidget($el1).show();
        layerWidget($el2).show();
        layerWidget($el3).show();
        layerWidget($el4).show();

        equal(this.dimSpy.callCount, 2, 'Dim should have been called twice');
        equal(this.undimSpy.callCount, 1, 'Undim should have been called once');
    });

    test('Layer pops down to a given element when many are layered', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer();

        layerWidget($el1).show();
        layerWidget($el2).show();
        layerWidget($el1).hide();

        equal($el1.attr('aria-hidden'), 'true', 'All layers should have been hidden');
        equal($el2.attr('aria-hidden'), 'true', 'All layers should have been hidden');
    });

    test('Layer pop multilayer', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({modal: true});
        var $el3 = this.createLayer({blanketed: true});

        layerWidget($el1).show();
        layerWidget($el2).show();
        layerWidget($el3).show();
        layerWidget($el3).hide();

        equal(this.dimSpy.callCount, 3, 'Dim should have been called thrice');
        equal(this.dimSpy.args[0][1], 2980, 'Dim should have been called with specific z-index');
        equal(this.dimSpy.args[1][1], 3180, 'Dim should have been called with specific z-index');
        equal(this.dimSpy.args[2][1], 2980, 'Dim should have been called with specific z-index');
    });

    test('Layer popTop with empty', function() {
        ok(!LayerManager.global.popTopIfNonPersistent(), 'Expected null response');
    });

    test('Layer popTop one element', function() {
        var $el = this.createLayer();
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popTopIfNonPersistent();

        strictEqual($topLayer.attr('aria-hidden'), 'true', 'Expected first element hidden');
    });

    test('Layer popTop two elements', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer();
        layerWidget($el1).show();
        layerWidget($el2).show();

        var $topLayer = LayerManager.global.popTopIfNonPersistent();

        equal($topLayer[0], $el2[0], 'Expected second element to be returned');
        strictEqual($el1.attr('aria-hidden'), 'false', 'Expected first element visible');
        strictEqual($el2.attr('aria-hidden'), 'true', 'Expected second element hidden');
    });

    test('Layer popTop modal', function() {
        var $el = this.createLayer().attr('data-aui-modal', true);
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popTopIfNonPersistent();

        ok(!$topLayer, 'Expected no element to be returned');
        strictEqual($el.attr('aria-hidden'), 'false', 'Expected first element visible');
    });

    test('Layer popTop persistent', function() {
        var $el = this.createLayer({persistent: true});
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popTopIfNonPersistent();

        ok(!$topLayer, 'Expected no element to be returned');
        strictEqual($el.attr('aria-hidden'), 'false', 'Expected first element visible');
    });

    test('Layer popUntilTopBlanketed with empty', function() {
        ok(!LayerManager.global.popUntilTopBlanketed(), 'Expected null response');
    });

    test('Layer popUntilTopBlanketed one element', function() {
        var $el = this.createLayer({blanketed: true});
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el[0], 'Expected single element to be returned');
        strictEqual($el.attr('aria-hidden'), 'true', 'Expected first element hidden');
    });

    test('Layer popUntilTopBlanketed with no blanketed element', function() {
        var $el = this.createLayer();
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popUntilTopBlanketed();

        ok(!$topLayer, 'Expected no element to be returned');
        strictEqual($el.attr('aria-hidden'), 'false', 'Expected first element visible');
    });

    test('Layer popUntilTopBlanketed two elements', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({blanketed: true});
        layerWidget($el1).show();
        layerWidget($el2).show();

        var $topLayer = LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el2[0], 'Expected second element to be returned');
        strictEqual($el1.attr('aria-hidden'), 'false', 'Expected first element visible');
        strictEqual($el2.attr('aria-hidden'), 'true', 'Expected second element hidden');
    });

    test('Layer popUntilTopBlanketed with layered non-blanketed elements', function() {
        var $el1 = this.createLayer({blanketed: true});
        var $el2 = this.createLayer({blanketed: true});
        var $el3 = this.createLayer();
        layerWidget($el1).show();
        layerWidget($el2).show();
        layerWidget($el3).show();

        var $topLayer = LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el2[0], 'Expected top blanketed element to be returned');
        strictEqual($el1.attr('aria-hidden'), 'false', 'Expected first element visible');
        strictEqual($el2.attr('aria-hidden'), 'true', 'Expected second element hidden');
        strictEqual($el3.attr('aria-hidden'), 'true', 'Expected third element hidden');
    });

    test('Layer popUntilTopBlanketed modal', function() {
        var $el = this.createLayer({
            blanketed: true,
            modal: true
        });
        layerWidget($el).show();

        var $topLayer = LayerManager.global.popUntilTopBlanketed();

        ok(!$topLayer, 'Expected no element to be returned');
        strictEqual($el.attr('aria-hidden'), 'false', 'Expected first element visible');
    });

    module('Layer - events', modulePrototype);

    test('Layer show triggers show event', function() {
        var layer = layerWidget(this.createLayer());
        var spy = sinon.spy();
        layer.on('show', spy);

        layer.show();

        ok(spy.calledOnce, 'Bound show event handler was called');
    });

    test('Layer hide triggers hide event', function() {
        var layer = layerWidget(this.createLayer());
        var spy = sinon.spy();
        layer.on('hide', spy);
        layer.show();

        layer.hide();

        ok(spy.calledOnce, 'Bound hide event handler was called');
    });

    test('Layer instance events can be turned off', function() {
        var layer = layerWidget(this.createLayer());
        var spy = sinon.spy();
        layer.on('hide', spy);
        layer.off('hide', spy);

        layer.show();
        layer.hide();

        ok(spy.notCalled, 'Expected event listener called');
    });

    test('Layer hide can trigger multiple hide events', function() {
        var layer1 = layerWidget(this.createLayer({blanketed: true}));
        var spy1 = sinon.spy();
        layer1.on('hide', spy1).show();
        var layer2 = layerWidget(this.createLayer());
        var spy2 = sinon.spy();
        layer2.on('hide', spy2).show();

        layer1.hide();

        ok(spy1.calledOnce, 'Bound hide event handler was called');
        ok(spy2.calledOnce, 'Bound hide event handler was called');
    });

    test('Layer show triggers global event', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var spy = sinon.spy();
        layerWidget.on('show', spy);

        layer.show();

        ok(spy.calledOnce, 'Global show event handler was called');
        var $boundEl = spy.args[0][1];
        equal($boundEl[0], $el[0], 'Global show event handler was called with single arg');
    });

    test('Layer show triggers global event for correct component', function() {
        var $el1 = this.createLayer();
        $el1.addClass('component1');

        var layer = layerWidget($el1);

        var componentOneStub = sinon.stub().returns(true);
        var componentTwoStub = sinon.stub().returns(true);
        var allStub = sinon.stub().returns(true);

        layerWidget.on('show', '.component1', componentOneStub);
        layerWidget.on('show', '.component2', componentTwoStub);
        layerWidget.on('show', allStub);

        layer.show();

        ok(componentOneStub.calledOnce, 'Expected bound handler was called');
        ok(componentTwoStub.notCalled, 'Expected bound handler was not called');
        ok(allStub.calledOnce, 'Handler for all was called once');
        var $boundEl = componentOneStub.args[0][1];
        equal($boundEl[0], $el1[0], 'Global hide event handler was called with single arg');
    });

    test('Layer hide triggers global event', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var spy = sinon.spy();
        layerWidget.on('hide', spy);
        layer.show();

        layer.hide();

        ok(spy.calledOnce, 'Global hide event handler was called');
        var $boundEl = spy.args[0][1];
        equal($boundEl[0], $el[0], 'Global hide event handler was called with single arg');
    });

    test('Layer hide triggers global event for correct component', function() {
        var $el1 = this.createLayer();
        $el1.addClass('component1');

        var layer = layerWidget($el1);

        var componentOneStub = sinon.stub().returns(true);
        var componentTwoStub = sinon.stub().returns(true);
        var allStub = sinon.stub().returns(true);

        layerWidget.on('hide', '.component1', componentOneStub);
        layerWidget.on('hide', '.component2', componentTwoStub);
        layerWidget.on('hide', allStub);

        layer.show();
        layer.hide();

        ok(componentOneStub.calledOnce, 'Expected bound handler was called');
        ok(componentTwoStub.notCalled, 'Expected bound handler was not called');
        ok(allStub.calledOnce, 'Handler for all was called once');
        var $boundEl = componentOneStub.args[0][1];
        equal($boundEl[0], $el1[0], 'Global hide event handler was called with single arg');
    });

    test('Layer global events can be turned off', function() {
        var layer = layerWidget(this.createLayer());
        var spy = sinon.spy();

        layerWidget.on('hide', spy);
        layerWidget.off('hide', spy);
        layer.show();
        layer.hide();

        ok(spy.notCalled, 'Expected event listener called');
    });

    test('Layer two global events turned off correctly, same event name', function() {
        var layer = layerWidget(this.createLayer());
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        layerWidget.on('hide', spy);
        layerWidget.on('hide', spy2);
        layerWidget.off('hide', spy);
        layer.show();
        layer.hide();
        layerWidget.off('hide', spy2);

        ok(spy.notCalled, 'Expected event listener called spy1');
        ok(spy2.calledOnce, 'Expected event listener called spy2');
    });

    test('Layer beforeShow event displays', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(true);

        layer.on('beforeShow', stub);
        layer.show();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(layer.isVisible(), 'Expected first element visible');
    });

    test('Layer beforeShow event cancels display', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);

        layer.on('beforeShow', stub);
        layer.show();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(!layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer beforeShow event cancels display with multiple handlers', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        layer.on('beforeShow', stub);
        layer.on('beforeShow', stub2);

        layer.show();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(stub2.calledOnce, 'Expected bound handler was called');
        ok(!layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer beforeHide event hides', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(true);
        layer.on('beforeHide', stub);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(!layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer beforeHide event cancels hide', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        layer.on('beforeHide', stub);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(layer.isVisible(), 'Expected first element hidden');
    });


    test('Layer beforeHide event cancels hide with multiple handlers', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(true);
        var stub2 = sinon.stub().returns(false);
        layer.on('beforeHide', stub);
        layer.on('beforeHide', stub2);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(stub2.calledOnce, 'Expected bound handler was called');
        ok(layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer global beforeHide event fires for correct component', function() {
        var $el1 = this.createLayer();
        $el1.addClass('component1');

        var layer = layerWidget($el1);

        var componentOneStub = sinon.stub().returns(true);
        var componentTwoStub = sinon.stub().returns(true);
        var allStub = sinon.stub().returns(true);

        layerWidget.on('beforeHide', '.component1', componentOneStub);
        layerWidget.on('beforeHide', '.component2', componentTwoStub);
        layerWidget.on('beforeHide', allStub);

        layer.show();
        layer.hide();

        ok(componentOneStub.calledOnce, 'Expected bound handler was called');
        ok(componentTwoStub.notCalled, 'Expected bound handler was not called');
        ok(allStub.calledOnce, 'Handler for all was called once');
    });

    test('Layer global beforeHide event cancels hide', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        layerWidget.on('beforeHide', stub);

        layer.show();
        layer.hide();

        layerWidget.off('beforeHide', stub);

        ok(stub.calledOnce, 'Expected bound handler was not called');
        ok(layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer global beforeHide event cancels hide with multiple handlers', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        layerWidget.on('beforeHide', stub);
        layerWidget.on('beforeHide', stub2);

        layer.show();
        layer.hide();

        layerWidget.off('beforeHide', stub);
        layerWidget.off('beforeHide', stub2);

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(stub2.calledOnce, 'Expected bound handler was called');
        ok(layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer global beforeShow event fires for correct component', function() {
        var $el1 = this.createLayer();
        $el1.addClass('component1');

        var layer = layerWidget($el1);

        var componentOneStub = sinon.stub().returns(true);
        var componentTwoStub = sinon.stub().returns(true);
        var allStub = sinon.stub().returns(true);

        layerWidget.on('beforeShow', '.component1', componentOneStub);
        layerWidget.on('beforeShow', '.component2', componentTwoStub);
        layerWidget.on('beforeShow', allStub);

        layer.hide();
        layer.show();

        ok(componentOneStub.calledOnce, 'Expected bound handler was called');
        ok(componentTwoStub.notCalled, 'Expected bound handler was not called');
        ok(allStub.calledOnce, 'Handler for all was called once');
    });

    test('Layer global beforeShow event cancels show', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        layerWidget.on('beforeShow', stub);

        layer.hide();
        layer.show();

        layerWidget.off('beforeShow', stub);

        ok(stub.calledOnce, 'Expected bound handler was not called');
        ok(!layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer global beforeShow event cancels show with multiple handlers', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        layerWidget.on('beforeShow', stub);
        layerWidget.on('beforeShow', stub2);

        layer.hide();
        layer.show();

        layerWidget.off('beforeShow', stub);
        layerWidget.off('beforeShow', stub2);

        ok(stub.calledOnce, 'Expected bound handler was called');
        ok(stub2.calledOnce, 'Expected bound handler was called');
        ok(!layer.isVisible(), 'Expected first element hidden');
    });


    module('Layer - visibility', modulePrototype);

    test('Layer isVisible returns true', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);

        layer.hide();

        ok(!layer.isVisible(), 'Expected first element hidden');
    });

    test('Layer isVisible returns true', function() {
        var $el = this.createLayer();
        var layer = layerWidget($el);

        layer.show();

        ok(layer.isVisible(), 'Expected first element visible');
    });


    module('Layer - traversing', modulePrototype);

    test('down() returns the layer below the current one', function() {
        var $layer1 = this.createLayer({blanketed: true});
        var $layer2 = this.createLayer();

        layerWidget($layer1).show();
        layerWidget($layer2).show();

        ok(layerWidget($layer2).below().is($layer1));
    });

    test('up() returns the layer above the current one', function() {
        var $layer1 = this.createLayer();
        var $layer2 = this.createLayer();

        layerWidget($layer1).show();
        layerWidget($layer2).show();

        ok(layerWidget($layer1).above().is($layer2));
    });


    module('Layer - native events', modulePrototype);

    test('Layer instance show events on native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);

        var spy = sinon.spy();

        onEvent($el1[0], 'show', spy);
        layer.show();
        layer.hide();
        offEvent($el1[0], 'show', spy);

        ok(spy.calledOnce, 'Expected event listener called');
    });

    test('Layer instance hide events on native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);

        var spy = sinon.spy();

        onEvent($el1[0], 'hide', spy);
        layer.show();
        layer.hide();
        offEvent($el1[0], 'hide', spy);

        ok(spy.calledOnce, 'Expected event listener called');
    });

    test('Layer global show event native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);
        var spy = sinon.spy();

        onEvent(document, 'show', spy);
        layer.show();
        offEvent(document, 'show', spy);

        ok(spy.calledOnce, 'Expected event listener called');
    });

    test('Layer global hide event native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);
        var spy = sinon.spy();

        onEvent(document, 'hide', spy);
        layer.show();
        layer.hide();
        offEvent(document, 'hide', spy);

        ok(spy.calledOnce, 'Expected event listener called');
    });

    test('Layer Global hide event cancels hide native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);

        var callCount = 0;
        var cancelEvent = function(e) {
            callCount++;
            e.preventDefault();
        };

        onEvent(document, 'hide', cancelEvent);
        layer.show();
        layer.hide();
        offEvent(document, 'hide', cancelEvent);

        ok(callCount === 1, 'Expected bound handler was not called');
        ok(layerWidget($el1).isVisible(), 'Expected first element hidden');
    });

    test('Layer global show event cancels show native', function() {
        var $el1 = this.createLayer();
        var layer = dialog2Widget($el1);

        var callCount = 0;
        var cancelEvent = function(e) {
            callCount++;
            e.preventDefault();
        };

        onEvent(document, 'show', cancelEvent);
        layer.hide();
        layer.show();
        offEvent(document, 'show', cancelEvent);

        ok(callCount === 1, 'Expected bound handler was not called');
        ok(!layerWidget($el1).isVisible(), 'Expected first element hidden');
    });
});
