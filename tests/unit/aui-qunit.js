(function($) {
    'use strict';

    function dispatch (event, target) {
        var orig = target;
        target = (typeof target === 'string') ? $(target) : target;
        target = (target instanceof $) ? target[0] : target;

        if (!target || typeof target.dispatchEvent !== 'function') {
            var msg = AJS.format('The object provided to dispatch events to did not resolve to a DOM element: was {0}', String(orig));
            var err = new Error(msg);
            err.target = target;
            throw err;
        }

        target.dispatchEvent(event);
    }

    function pressKey (keyCode, modifiers) {
        var e = new CustomEvent('keydown', { bubbles: true, cancelable: true });
        modifiers = modifiers || {};
        e.keyCode = keyCode;
        e.ctrlKey = !!modifiers.control;
        e.shiftKey = !!modifiers.shift;
        e.altKey = !!modifiers.alt;
        e.metaKey = !!modifiers.meta;
        dispatch(e, document.activeElement);
    }

    function click (element) {
        var e = new CustomEvent('click', { bubbles: true, cancelable: true });
        dispatch(e, element);
    }

    function hover (element) {
        var hoverEvents = ['mouseenter','mouseover','mousemove'];
        $.each(hoverEvents, function(i, name) {
            var e = new CustomEvent(name, { bubbles: true, cancelable: true });
            dispatch(e, element);
        });
    }

    function addFixtureToBody () {
        $('<div id="qunit-fixture"></div>').appendTo(document.body);
    }

    function removeFixtureFromBody () {
        $('#qunit-fixture').remove();
    }

    function getLayers () {
        return $('.aui-layer');
    }

    function warnIfLayersExist(test) {
        if (getLayers().length) {
            // Need to bind to console for chrome, otherwise it throws an illegal invocation error.
            console.log(
                test.module +
                    ': ' +
                    test.name +
                    ' - ' +
                    'Layers have been left in the DOM. These must be removed from the BODY to ensure they don\'t affect other tests. Use AJS.qunit.removeLayers().'
            );
            removeLayers();
        }
    }


    function createFixtureItems (fixtureItems, removeOldFixtures) {
        var fixtureElement = document.getElementById('qunit-fixture');

        if (removeOldFixtures || removeOldFixtures === undefined) {
            fixtureElement.innerHTML = '';
        }

        if (fixtureItems) {
            for (var name in fixtureItems) {
                fixtureItems[name] = $(fixtureItems[name]).get(0);
                fixtureElement.appendChild(fixtureItems[name]);
            }
        }

        return fixtureItems;
    }

    function removeLayers () {
        var $layer;

        while ($layer = AJS.LayerManager.global.getTopLayer()) {
            AJS.LayerManager.global.popUntil($layer);
            $layer.remove();
        }

        getLayers().remove();
        AJS.undim();
        $('.aui-blanket').remove();
    }


    // Karma does not create a qunit-fixture element by default - use this to setup and teardown the fixture.
    QUnit.testStart(function (test) {
        addFixtureToBody();
    });

    QUnit.testDone(function (test) {
        warnIfLayersExist(test);
        removeFixtureFromBody();
    });

    AJS.qunit = {
        fixtures: createFixtureItems,
        removeLayers: removeLayers,
        click: click,
        hover: hover,
        pressKey: pressKey
    };

})(AJS.$);
