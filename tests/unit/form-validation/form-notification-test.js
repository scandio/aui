define(['jquery', 'aui/internal/skate', 'form-notification', 'aui-qunit'], function($, skate) {
    'use strict';
    module('Form notification Unit Tests', {
        createInput: function(attributes) {
            var $input = AJS.$('<input type="text">');

            attributes = $.extend({}, attributes, {'data-aui-notification-field': ''});

            $.each(attributes, function(key, value){
                $input.attr(key, value);
            });
            $('#qunit-fixture').append($input);
            skate.init($input);
            return $input;
        },
        countTipsysOnPage: function() {
            return $('.tipsy').length;
        },
        firstTipsyOnPage: function() {
            return $('.tipsy').first();
        },
        setup: function() {
            this.clock = sinon.useFakeTimers();
        },
        setupLinkEnvironment: function() {
            return this.createInput({
                'data-aui-notification-info': '["Some text here followed by a link <a href="http://google.com/">click here</a>"]'
            });
        },
        teardown: function() {
            $('.tipsy').remove();
            this.clock.restore();
        }
    });

    test('Fields can receive notifications', function() {
        var $input = this.createInput({
            'data-aui-notification-info': 'Test info message'
        });

        strictEqual(this.countTipsysOnPage(), 0, 'No notification is shown before focusing on a field');
        $input.focus();
        strictEqual(this.countTipsysOnPage(), 1, 'A notification is shown after focusing on a field');
    });

    test('Field notifications receive correct notification class', function() {
        var $input = this.createInput({
            'data-aui-notification-error': 'Test error message'
        });
        $input.focus();
        strictEqual($('.aui-form-notification-tooltip-error').length, 1, 'An error tipsy is shown for error notifications');
        strictEqual($('.aui-form-notification-tooltip-info').length, 0, 'No info tipsy is shown for error notifications');
    });

    test('Field notification messages stack correclty', function() {
        var $input = this.createInput({
            'data-aui-notification-info': '["Test JSON message 1","Test JSON message 2"]'
        });
        $input.focus();
        ok($('.tipsy-inner')[0].innerHTML.indexOf('<li>') !== -1, 'The message contains a list item if a JSON stacked message is given');
    });

    test('There is only one tooltip on the page after focusing multiple', function() {
        var $input1 = this.createInput({
            'data-aui-notification-info': 'The first message'
        });

        var $input2 = this.createInput({
            'data-aui-notification-info': 'The second message'
        });
        strictEqual(this.countTipsysOnPage(), 0, 'Initially there are no tipsys on the page');
        $input1.focus();
        strictEqual(this.countTipsysOnPage(), 1, 'After focusing the first field one tipsy is present');
        $input1.blur();
        $input2.focus();
        strictEqual(this.countTipsysOnPage(), 1, 'After focusing the second field one tipsy is present');

    });
    test('Field notification links are followed', function() {
        var $input = this.setupLinkEnvironment();
        this.pageRedirect = sinon.stub();
        $(window).on('beforeunload', this.pageRedirect);

        $input.focus();
        strictEqual(this.countTipsysOnPage(), 1, 'Tipsy is open');

        $('a').first()[0].dispatchEvent(new CustomEvent('click',{
            cancelable: true
        }));

        strictEqual(this.pageRedirect.callCount, 1, 'Link has been followed');
    });

    test('Clicking on the notification when the field is focused keeps the notification open', function() {
        var $input = this.setupLinkEnvironment();
        $input.focus();
        var $tipsy = this.firstTipsyOnPage();
        $tipsy.click();
        strictEqual(this.countTipsysOnPage(), 1, 'A tipsy is visible on the page after clicking on the tooltip');
    });

    test('Notification states can be changed after a field is created', function() {
        var $input = this.createInput({
            'data-aui-notification-info': 'Test info message'
        });
        $input.focus();
        $input.attr('data-aui-notification-error', 'Test error message');
        strictEqual($('.aui-form-notification-tooltip-error').length, 1, 'An error tipsy is shown after adding one to an info field');
    });

});
