define(['aui-experimental-tooltip', 'aui-qunit'], function () {
    var $el;
    var TIPSY_DELAY = 10;

    function visibleTipsies() {
        return $(document.body).find('.tipsy').filter(':visible');
    }

    module("Tooltips unit test", {
        setup: function () {
            this.clock = sinon.useFakeTimers();
            $el = $("<div title='my element'>Element</div>");
            $(document.body).append($el);
            $el.tooltip({
                delayIn: TIPSY_DELAY
            });
        },
        teardown: function () {
            this.clock.restore();
            $el.remove();
            $(document.body).find(".tipsy").remove();
        }
    });

    test("Delayed tooltip shows up", function () {
        equal(visibleTipsies().length, 0, "Should be no tipsy");

        AJS.qunit.hover($el);

        this.clock.tick(1);
        equal(visibleTipsies().length, 0);

        this.clock.tick(TIPSY_DELAY);
        var $tipsies = visibleTipsies();
        equal($tipsies.length, 1, "Tipsy should be showing");

        var position = $tipsies.position();
        notEqual(position.left, 0, "Tipsy should not be top left");
        notEqual(position.top, 0, "Tipsy should not be top left");
    });

    test("Delayed tooltip doesn't show if underlying element is gone", function () {
        equal(visibleTipsies().length, 0, "Should be no tipsy");

        AJS.qunit.hover($el);

        $el.remove();

        this.clock.tick(TIPSY_DELAY);
        equal(visibleTipsies().length, 0, "Tipsy should NOT be showing");
        equal($(".tipsy").length, 0, "There should be no Tipsies in the DOM");
    });

});