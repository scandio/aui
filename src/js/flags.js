;(function(init) {
    'use strict';
    if (typeof window.define === 'function' && window.define.amd) {
        define(['aui', 'jquery', 'template', 'internal/browser', 'raf'], init);
    } else {
        AJS.flag = init(AJS, AJS.$, AJS.template, AJS._internal.browser);
    }
})(function (AJS, $, template, browser) {
    'use strict';

    var ID_FLAG_CONTAINER = 'aui-flag-container';

    function flag (options) {
        var isMessageCloseable = !options.persistent;
        var $flag = renderFlagElement(options);

        if (isMessageCloseable) {
            makeCloseable($flag);
        }

        pruneFlagContainer();
        insertFlag($flag);
    }

    function renderFlagElement(options) {
        var html =
            '<div class="aui-flag">' +
                '<div class="aui-message aui-message-{type} {type} {persistent} shadowed">' +
                    '<p class="title">' +
                    '<strong>{title}</strong>' +
                    '</p>' +
                    '{body}<!-- .aui-message -->' +
                '</div>' +
            '</div>';

        var $flag = $(template(html).fill({
            type: options.type || 'info',
            persistent: options.persistent ? '' : 'closeable',
            title: options.title || '',
            'body:html': options.body || ''
        }).toString());

        return $flag;
    }

    function makeCloseable($flag) {
        var $icon = $('<span class="aui-icon icon-close" role="button" tabindex="0"></span>');
        $icon.click(function () {
            closeFlag($flag);
        });
        $icon.keypress(function(e) {
            if ((e.which === AJS.keyCode.ENTER) || (e.which === AJS.keyCode.SPACE)) {
                closeFlag($flag);
                e.preventDefault();
            }
        });

        $flag.find('.aui-message').append($icon);
    }

    function closeFlag($flagToClose) {
        $flagToClose.attr('aria-hidden', 'true');
    }

    function pruneFlagContainer() {
        var $container = findContainer();
        var $allFlags = $container.find('.aui-flag');

        $allFlags.get().forEach(function(flag) {
            var isFlagAriaHidden = flag.getAttribute('aria-hidden') === 'true';
            if (isFlagAriaHidden) {
                $(flag).remove();
            }
        });
    }

    function findContainer() {
        return $('#' + ID_FLAG_CONTAINER);
    }

    function insertFlag($flag) {
        var $flagContainer = findContainer();
        if (!$flagContainer.length) {
            $flagContainer = $('<div id="' + ID_FLAG_CONTAINER + '"></div>');
            $('body').prepend($flagContainer);
        }

        insertMessageAndRequestFrames($flag);
    }

    function insertMessageAndRequestFrames($flag) {
        var $flagContainer = findContainer();
        if (browser.supportsRequestAnimationFrame()) {
            requestAnimationFrame(function() {
                $flag.appendTo($flagContainer);
                requestAnimationFrame(function() {
                    $flag.attr('aria-hidden', 'false');
                });
            });
        }
        else {
            $flag.appendTo($flagContainer);
            $flag.attr('aria-hidden', 'false');
        }
    }

    return flag;
});
