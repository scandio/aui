define(['jquery', 'aui/internal/skate', 'aui-navigation', 'aui-qunit'], function($, skate) {
    module("Navigation Unit Tests", {
        setup : function() {
            this.$fixture =  $("#qunit-fixture");
            $('<ul class="aui-nav"><li aria-expanded="false">' +
                '<a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>' +
                '<a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>' +
                '<ul class="aui-nav" title="one" data-more="" data-more-limit="3">' +
                '<li class="aui-nav-selected"><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>' +
                '</ul></li></ul>' +
                '<ul class="aui-nav"><li aria-expanded="false">' +
                '<a href="#" class="aui-nav-subtree-toggle"><span class="aui-icon aui-icon-small aui-iconfont-expanded"></span></a>' +
                '<a href="#" class="aui-nav-item"><span class="aui-icon aui-icon-small aui-iconfont-time"></span><span class="aui-nav-item-label">ITEM</span></a>' +
                '<ul class="aui-nav" title="one" data-more="" data-more-limit="3">' +
                '<li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li><li><a href="home.html" class="aui-nav-item"><span class="aui-nav-item-label">ITEM</span></a></li>' +
                '</ul></li></ul>').appendTo(this.$fixture);
            skate.init(this.$fixture);
        }
    });

    // Expander tests
    test("Expander", function() {
        var $toggle = this.$fixture.find('>.aui-nav:first .aui-nav-subtree-toggle'),
            $expander = this.$fixture.find('>.aui-nav:first li[aria-expanded]');

        strictEqual($expander.attr('aria-expanded'), 'true', 'Auto-expand when a child is selected');
        $toggle.click();
        strictEqual($expander.attr('aria-expanded'), 'false', 'Collapse on click');
        $toggle.click();
        strictEqual($expander.attr('aria-expanded'), 'true', 'Expand on click');
    });

    // Make sure aui-nav-child-selected class is added
    test("Child Selected class", function() {
        ok(this.$fixture.find('.aui-nav-selected').closest('li:not(.aui-nav-selected)').hasClass('aui-nav-child-selected'));
    });

    // 'More...' tests
    test("More Initialisation", function() {
        ok(this.$fixture.find('>.aui-nav:last .aui-nav-more').length);
    });

    test("No More Initialisation if selected", function() {
        ok(this.$fixture.find('>.aui-nav:first .aui-nav-more').length === 0);
    });

    test("More Expansion", function() {
        this.$fixture.find('>.aui-nav:last .aui-nav-more a').click();

        ok(this.$fixture.find('>.aui-nav:last .aui-nav-more').length === 0);
    });

    test("Show 'More' again when re-expanded", function() {
        var $toggle = this.$fixture.find('>.aui-nav:last .aui-nav-subtree-toggle');

    	this.$fixture.find('>.aui-nav:last .aui-nav-more a').click();

    	ok(this.$fixture.find('>.aui-nav:last .aui-nav-more').length === 0);

        $toggle.click();
        $toggle.click();

        ok(this.$fixture.find('.aui-nav-more').length);
    });
});
