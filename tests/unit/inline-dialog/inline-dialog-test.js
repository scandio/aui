define(["inline-dialog", 'aui-qunit'], function() {
    'use strict';
    /*jshint quotmark:false */

    var layers = [];

    module("Inline Dialog Options", {
        setup: function() {
            this.$link = AJS.$("<div class='dialog-trigger'></div>").appendTo("#qunit-fixture");
        },
        createDialog: function(options) {
            var dialog;
            if (typeof options === 'undefined'){
                dialog = AJS.InlineDialog(this.$link, 1, function(){});
            } else {
                dialog = AJS.InlineDialog(this.$link, 1, function(){}, options);
            }
            layers.push(dialog);
            return dialog;
        },
        teardown: function() {
            layers.forEach(function(el) {
                el.remove();
            });
        }
    });

    test("Inline Dialog Creation", function() {
        var testInlineDialog = this.createDialog();
        ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
        ok(typeof testInlineDialog.show == "function", "testInlineDialog has the show function.");
        ok(typeof testInlineDialog.hide == "function", "testInlineDialog has the hide function.");
        ok(typeof testInlineDialog.refresh == "function", "testInlineDialog has the refresh function.");
    });

    test("Inline Dialog getOptions function", function() {
        var testInlineDialog = this.createDialog();
        ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
        ok(typeof testInlineDialog.show == "function", "testInlineDialog has the show function.");
        ok(typeof testInlineDialog.hide == "function", "testInlineDialog has the hide function.");
        ok(typeof testInlineDialog.refresh == "function", "testInlineDialog has the refresh function.");
        ok(typeof testInlineDialog.getOptions == "function", "testInlineDialog has the getOptions function");
        ok(typeof testInlineDialog.getOptions() == "object", "getOptions function successfully returned an object");
    });
    test("Inline Dialog default options", function() {
        var testInlineDialog = this.createDialog();
        var expectedDefaults = {
            onTop: false,
            responseHandler: function(data, status, xhr) {
                //assume data is html
                return data;
            },
            closeOthers: true,
            isRelativeToMouse: false,
            addActiveClass: true,
            onHover: false,
            useLiveEvents: false,
            noBind: false,
            fadeTime: 100,
            persistent: false,
            hideDelay: 10000,
            showDelay: 0,
            width: 300,
            offsetX: 0,
            offsetY: 10,
            arrowOffsetX: 0,
            arrowOffsetY: 0,
            container: "body",
            cacheContent : true,
            displayShadow: true,
            autoWidth: false,
            gravity: 'n',
            closeOnTriggerClick: false,
            preHideCallback: function () { return true; },
            hideCallback: function(){}, // if defined, this method will be exected after the popup has been faded out.
            initCallback: function(){}, // A function called after the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
            upfrontCallback: function() {}, // A function called before the popup contents are loaded. `this` will be the popup jQuery object, and the first argument is the popup identifier.
            calculatePositions: function() {},
            getArrowPath: function() {},
            getArrowAttributes: function() {}
        };
        ok(typeof testInlineDialog == "object", "Inline Dialog Successfully Created!");
        var isDefault = true;
        // console.log(isDefault);
        AJS.$.each(testInlineDialog.getOptions(), function(index, value){
            if(expectedDefaults[index] != value && typeof value != "function" || typeof value != typeof expectedDefaults[index]){
                isDefault = false;
            }
        });
        // console.log(isDefault);
        ok(isDefault, "all default options are as expected");

    });

    test('Inline dialog displayShadow option', function(){
        var testInlineDialog = this.createDialog({displayShadow: false});
        this.$link.click();

        var dialogNoShadow = testInlineDialog.find('.contents').hasClass('aui-inline-dialog-no-shadow');

        strictEqual(true, dialogNoShadow,'Dialog removes shadow css when displayShadow option is false');
    });

    test('Inline dialog autoWidth option', function(){
        var testInlineDialog = this.createDialog({autoWidth: true});
        this.$link.click();

        var dialogAutoWidth = testInlineDialog.find('.contents').hasClass('aui-inline-dialog-auto-width');

        strictEqual(true, dialogAutoWidth, 'Dialog adds autowidth css when autowidth option is true');
    });

    test('Inline dialog show with no options', function(){
        var testInlineDialog = this.createDialog();
        testInlineDialog.show();
        strictEqual($(testInlineDialog).css('display'), 'block');
    });


    module("Inline Dialog Positioning", {
        setup: function() {
            var popup = this.popup = AJS.$("<div id='dummy-popup'><span class='arrow'/></div>")
                .css({"width": "200px", "height": "100px"});

            var trigger = this.trigger = AJS.$("<span id='dummy-trigger'/>")
                .appendTo(AJS.$("#qunit-fixture"))
                .css({"width": "200px", "height": "20px", "display": "inline-block", "position": "fixed", "left": "10px", "top": "10px"});

            var mouse = this.mouse = { x: 0, y: 0 };
            var opts = this.opts = { offsetX: 0, offsetY: 0, arrowOffsetX: 0 }; // Will get NaN unless these are supplied. wtf.

            this.invoke = function() {
                return AJS.InlineDialog.opts.calculatePositions(popup, { target: trigger }, mouse, opts);
            };
        }
    });

    test("Returns object with positioning information", function() {
        var result = this.invoke();
        equal(typeof result, "object");

        equal(typeof result.popupCss, "object");
        ok(result.popupCss.hasOwnProperty("left"));
        ok(result.popupCss.hasOwnProperty("right"));
        ok(result.popupCss.hasOwnProperty("top"));

        equal(typeof result.arrowCss, "object");
        ok(result.arrowCss.hasOwnProperty("left"));
        ok(result.arrowCss.hasOwnProperty("right"));
        ok(result.arrowCss.hasOwnProperty("top"));
    });

    test("Popup left-aligned with trigger's left edge when trigger is smaller", function() {
        this.trigger.width(100);
        this.trigger.css("left", 10);
        this.popup.width(200);

        var result = this.invoke();
        equal(result.popupCss.left, 10);
    });

    test("Popup centre-aligned to trigger's width when trigger is larger", function() {
        this.trigger.width(400);
        this.trigger.css("left", 10);
        this.opts.width = 200; // fixme: wtf, srsly
        this.popup.width(this.opts.width);

        var result = this.invoke();
        equal(result.popupCss.left, 110, "should start at half width of trigger minus half width of popup plus trigger's offset");
    });

    test("Popup cannot be positioned further than 10px from window's right edge when popup would cause horizontal scrollbars", function() {
        this.trigger.width(100);
        this.trigger.css({"left": "auto", "right": 10});
        this.popup.width(200);

        var result = this.invoke();
        equal(result.popupCss.left, "auto");
        equal(result.popupCss.right, 10);
    });

    test("Popup arrow points to middle of trigger when popup is left-aligned", function() {
        this.trigger.width(100);
        this.trigger.css("left", 10);
        this.popup.width(200);

        var result = this.invoke();
        equal(result.arrowCss.left, 50);
    });

    test("Popup arrow points to middle of trigger when popup is centre-aligned", function() {
        this.trigger.width(100);
        this.trigger.css("left", 10);
        this.popup.width(200);

        var result = this.invoke();
        equal(result.arrowCss.left, 50);
    });

    test("Popup arrow points to middle of trigger when popup is right-aligned", function() {
        this.trigger.width(100);
        this.trigger.css({"left": "auto", "right": 10});
        this.popup.width(200);

        var result = this.invoke();
        equal(result.arrowCss.left, 150, "you'd think it'd be on the right, but we don't do that, so it's popup width minus the offset.");
        equal(result.arrowCss.right, "auto");
    });

    module("Inline Dialog Hiding", {
        setup: function() {
            this.clock = sinon.useFakeTimers();
            AJS.$.fx.off = true;
            this.$link = AJS.$("<div class='test-link'></div>").css("display", "none");
            this.$popupContainer = AJS.$("<div class='popup-container'></div>");
            var $styles = AJS.$("<style type='text/css'>.aui-inline-dialog { display: none; }</style>");
            AJS.$("#qunit-fixture").append($styles).append(this.$link).append(this.$popupContainer);
        },
        teardown: function() {
            AJS.$.fx.off = false;
            this.clock.restore();
        },
        createDialog: function(options) {
            var defaultOptions = {
                container: '.popup-container'
            };
            var renderPopupContent = function (content, trigger, showPopup) {
                showPopup();
            };
            this.dialog = AJS.InlineDialog(this.$link, 1, renderPopupContent, AJS.$.extend({}, defaultOptions, options));
        },
        renderDialog: function(options) {
            this.createDialog(options);
            this.dialog.show();
            this.clock.tick();
        },
        pressEsc: function() {
            AJS.qunit.pressKey(AJS.keyCode.ESCAPE);
            this.clock.tick();
        },
        dialogVisible: function() {
            return this.dialog.css('display') === 'block';
        },
        dialogHidden: function() {
            return this.dialog.css('display') === 'none';
        },
        clickLink: function() {
            this._triggerEvent("click", 0);
        },
        hoverLink: function() {
            this._triggerEvent("mousemove", 0);
        },
        unhoverLink: function() {
            // 10000 is inline dialog default hideDelay
            this._triggerEvent("mouseout", 10000);
        },
        _triggerEvent: function(eventName, delay) {
            var e = jQuery.Event(eventName);
            this.$link.trigger(e);
            this.clock.tick(delay);
        }
    });

    test("Dialog show", function() {
        this.createDialog();
        this.dialog.show();
        this.clock.tick();

        ok(this.dialogVisible(), 'Expected dialog was shown');
    });

    test("Dialog hides when escape is pressed", function() {
        this.renderDialog();
        ok(this.dialogVisible(), 'the dialog should render as visible');

        this.pressEsc();
        ok(this.dialogHidden(), 'the dialog should be hidden once esc is pressed');
    });

    test("Dialog unregisters to keydown event when hidden", function() {
        expect(0);
        sinon.spy(AJS.$.prototype, 'off');
        this.renderDialog();
        this.pressEsc();

        sinon.assert.calledWith(AJS.$.prototype.off, 'keydown', sinon.match.func);
    });

    test("Dialog does not hide when esc is pressed and persistent is enabled", function() {
        this.renderDialog({persistent: true});
        this.pressEsc();

        ok(this.dialogVisible());
    });

    test("Dialog doesnt hide when trigger clicked and closeOnTriggerClick is false", function() {
        this.renderDialog({
            closeOnTriggerClick: false
        });

        this.clickLink();

        ok(this.dialogVisible(), 'Expected dialog was closed');
    });

    test("Dialog hides when trigger clicked and closeOnTriggerClick is true", function() {
        this.renderDialog({
            closeOnTriggerClick: true
        });

        this.clickLink();

        ok(!this.dialogVisible(), 'Expected dialog was closed');
    });

    test("Dialog does not show when trigger pressed if nobind is true", function() {
        this.renderDialog({
            noBind: true
        });
        this.clickLink();

        ok(this.dialogHidden(), 'Expected dialog was not shown');
    });

    test("Inline Dialog direct event binding (for click)", function() {
        this.createDialog();

        this.clickLink();
        ok(this.dialogVisible(), "Expected dialog was shown");

        this.unhoverLink();
        ok(this.dialogHidden(), "Expected dialog was hidden");
    });

    test("Inline Dialog direct event binding (for hover)", function() {
        this.createDialog({
            onHover: true
        });

        this.hoverLink();
        ok(this.dialogVisible(), "Expected dialog was shown");

        this.unhoverLink();
        ok(this.dialogHidden(), "Expected dialog was hidden");
    });

// .live() has been removed from jQuery 1.9+; InlineDialog.useLiveEvents is not supported in these jQuery versions
// so skip tests.
    if (AJS.$.fn.live) {

        test("Inline Dialog live event binding does not work for jQuery objects without a selector (for click)", function() {
            ok(!this.$link.selector, "Expected $el.selector does not exist");
            // jQuery live events require $el.selector to exist, so this will fail
            this.createDialog({
                useLiveEvents: true
            });

            this.clickLink();
            ok(!this.dialogVisible(), "Expected dialog was not shown");
        });

        test("Inline Dialog live event binding does not work for jQuery objects without a selector (for hover)", function() {
            ok(!this.$link.selector, "Expected $el.selector does not exist");
            // jQuery live events require $el.selector to exist, so this will fail
            this.createDialog({
                onHover: true,
                useLiveEvents: true
            });

            this.hoverLink();
            ok(!this.dialogVisible(), "Expected dialog was not shown");
        });

        test("Inline Dialog live event binding (for click)", function() {
            // jQuery live events require $el.selector to exist, so inline dialog can only work with jQuery objects that have a selector
            this.$link = AJS.$(".test-link");
            ok(this.$link.selector, "Expected $el.selector exists");
            this.createDialog({
                useLiveEvents: true
            });

            this.clickLink();
            ok(this.dialogVisible(), "Expected dialog was shown");

            this.unhoverLink();
            ok(this.dialogHidden(), "Expected dialog was hidden");
        });

        test("Inline Dialog live event binding (for hover)", function() {
            // jQuery live events require $el.selector to exist, so inline dialog can only work with jQuery objects that have a selector
            this.$link = AJS.$(".test-link");
            ok(this.$link.selector, "Expected $el.selector exists");
            this.createDialog({
                onHover: true,
                useLiveEvents: true
            });

            this.hoverLink();

            ok(this.dialogVisible(), "Expected dialog was shown");

            this.unhoverLink();
            ok(this.dialogHidden(), "Expected dialog was hidden");
        });
    }
});
