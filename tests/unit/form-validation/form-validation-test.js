define(['jquery', 'aui/internal/skate', 'form-validation', 'form-provided-validators', 'aui-qunit'], function($, skate, validator) {
    'use strict';

    function makeValidationInput(attributes, $parent, inputHTML) {
        var html = inputHTML ? inputHTML : '<input type="text">';
        var $input = $(html);

        attributes = $.extend({}, attributes, {'data-aui-validation-field': '', 'data-aui-validation-when': 'keyup'});

        $.each(attributes, function(key, value){
            $input.attr(key, value);
        });
        $parent.append($input);
        skate.init($input);
        skate.init($input);
        return $input;
    }

    function typeMessage($input, messageToType) {
        $input.val(messageToType);
        $input.trigger('keyup');
        $input.focus();
    }

    function fieldIsValid($input){
        return $input.attr('data-aui-validation-state') === 'valid';
    }

    function fieldIsInvalid($input){
        return $input.attr('data-aui-validation-state') === 'invalid';
    }

    function fieldIsUnvalidated($input){
        return $input.attr('data-aui-validation-state') === 'unvalidated';
    }

    function tipsyOnPage(){
        return $('.tipsy').length > 0;
    }

    function firstTipsyOnPageMessage() {
        return $('.tipsy').first().text();
    }

    function iconsOnPage(){
        return $('[data-aui-notification-error] + .aui-icon-notification').length + $('[data-aui-notification-info] + .aui-icon-notification').length;
    }

    function errorIconsOnPage() {
        return AJS.$('[data-aui-notification-error]').length;
    }

    function successIconsOnPage() {
        return AJS.$('[data-aui-notification-success]').length - AJS.$('[data-aui-notification-success][data-aui-notification-error]').length;
    }

    function waitIconsOnPage() {
        return AJS.$('.aui-icon-wait').length;
    }

    function validateInput($input) {
        validator.validate($input);
        this.clock.tick(1000); //Notifications appearing is not synchronous
    }

    module('Form validation Unit Tests', {
        makeValidationInput: makeValidationInput,
        typeMessage: typeMessage,
        fieldIsValid: fieldIsValid,
        fieldIsInvalid: fieldIsInvalid,
        fieldIsUnvalidated: fieldIsUnvalidated,
        tipsyOnPage: tipsyOnPage,
        firstTipsyOnPageMessage: firstTipsyOnPageMessage,
        iconsOnPage: iconsOnPage,
        errorIconsOnPage: errorIconsOnPage,
        successIconsOnPage: successIconsOnPage,
        waitIconsOnPage: waitIconsOnPage,
        validateInput: validateInput,

        teardown: function(){
            $(".tipsy").remove();
            this.clock.restore();
        },
        setup: function() {
            try {
                //Register validators for general usage
                validator.register(['alwaysinvalidate'], function(field) {
                    field.invalidate('Invalid');
                });
            } catch (e) {
                //we expect an error as we try to register this plugin every time setup is called.
                if (e.name !== 'FormValidationPluginError') {
                    throw e;
                }
            }
            this.clock = sinon.useFakeTimers();
        }
    });

    test('Input fields start with unvalidated class', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'));

        ok(this.fieldIsUnvalidated($input), 'Fields start with unvalidated class');
    });

    test('Input field becomes validated after typing valid message', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'));

        this.typeMessage($input, 'This is a message longer than twenty characters');
        ok(this.fieldIsValid($input), 'Input has become valid after typing valid input');
    });

    test('Input field becomes invalid after typing invalid message', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '100'
        }, $('#qunit-fixture'));

        this.typeMessage($input, 'This is a message shorter than one hundred characters');
        ok(this.fieldIsInvalid($input), 'Input has become invalid after typing invalid input');
    });

    test('Input field becomes invalid if one or two validation functions return invalid', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '5',
            'data-aui-validation-dateformat': 'Y-m-d'
        }, $('#qunit-fixture'));

        this.typeMessage($input, 'A');
        ok(this.fieldIsInvalid($input), 'Input has become invalid if two validators invalidate');
        this.typeMessage($input, 'AAAAAAAAAA');
        ok(this.fieldIsInvalid($input), 'Input has become invalid if one validator invalidates');
    });

    test('Tooltip is shown when field is invalidated', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-maxlength': '20'
        }, $('#qunit-fixture'));

        this.typeMessage($input, 'This is a message longer than twenty characters');
        ok(this.tipsyOnPage(), 'A tooltip is shown when a field becomes invalid');
    });

    test('Icon is shown when icons needing fields are invalidated', function() {
        var $textInput = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'), '<input type="text">');
        this.typeMessage($textInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a text input becomes invalid');

        this.typeMessage($textInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a text input becomes valid');


        var $passwordInput = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'), '<input type="password">');
        this.typeMessage($passwordInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a password input becomes invalid');

        this.typeMessage($passwordInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a password input becomes valid');

        var $textareaInput = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'), '<textarea>');
        this.typeMessage($textareaInput, 'Too short');
        strictEqual(this.iconsOnPage(), 1, 'An icon is shown when a textarea becomes invalid');

        this.typeMessage($textareaInput, 'This is a message longer than twenty characters');
        strictEqual(this.iconsOnPage(), 0, 'No icon is shown when a textarea becomes valid');
    });

    test('Invalidation is added to fields that are divs', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-alwaysinvalidate': '20'
        }, $('#qunit-fixture'), '<div>');

        this.validateInput($input);
        ok(this.fieldIsInvalid($input), 0, 'Non input fields still get invalid class');

    });

    test('Non text input fields tooltips are unaffected by blur', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-alwaysinvalidate': ''
        }, $('#qunit-fixture'), '<div>');

        this.validateInput($input);

        ok(this.tipsyOnPage(), 'Tooltip is present on page before blur on <div>');

        $input.focus();
        $input.blur();

        ok(this.tipsyOnPage(), 'Tooltip is present on page after blur on <div>');
    });

    test('Text input field tooltips are hidden after blur', function() {
        var $input = this.makeValidationInput({
            'data-aui-validation-alwaysinvalidate': ''
        }, $('#qunit-fixture'));

        this.validateInput($input);

        $input.focus();
        ok(this.tipsyOnPage(), 'Tooltip shown before blur on <input>');
        $input.blur();
        this.clock.tick(1000);
        ok(!this.tipsyOnPage(), 'Tooltip is hidden after blur on <input type="text">');

    });

    test('Plugged in validators can be used by fields', function() {
        validator.register(['testvalidator'], function(field){
            if (field.$el.val().length !== 3) {
                field.invalidate('not length three');
            } else {
                field.validate();
            }
        });

        var $input = this.makeValidationInput({
            'data-aui-validation-testvalidator': ''
        }, $('#qunit-fixture'));

        this.typeMessage($input, '1234');
        ok(this.fieldIsInvalid($input), 'Field is invalid when custom validator\'s conditions are not met');
        this.typeMessage($input, '123');
        ok(this.fieldIsValid($input), 'Field is valid when custom validator\'s conditions are met');

    });

    test('Manual revalidation works', function(){
        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '20'
        }, $('#qunit-fixture'));
        $input.val('too short');
        this.validateInput($input);
        ok(this.fieldIsInvalid($input), 'Field becomes invalid after manual validation');
    });

    test('Whole form validation triggers validsubmit', function() {
        expect(1);

        var $form = $('<form></form>');
        $('#qunit-fixture').append($form);

        var $input = this.makeValidationInput({
            'data-aui-validation-minlength': '1'
        }, $form);

        this.typeMessage($input, 'this message is long enough');

        $form.one('aui-valid-submit', function(e) {
            e.preventDefault();
            ok(true, 'Valid submit is triggered when the form is valid');
        });

        $form.trigger('submit');
    });

    test('A field with the "watchfield" argument should have validation triggered when the watched field would trigger validation', function() {
        var $input1 = AJS.$('<input id="input1"/>').appendTo('#qunit-fixture');
        var $input2 = this.makeValidationInput({
            'data-aui-validation-matchingfield': 'input1',
            'data-aui-validation-watchfield': 'input1'
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input1, 'mismatched message');
        this.typeMessage($input2, 'matching message');

        ok(this.fieldIsInvalid($input2), 'Input 2 is invalid when the fields don\'t match');
        this.typeMessage($input1, 'matching message');
        ok(this.fieldIsValid($input2), 'Input 2 becomes valid when the fields do match');
    });

    test('Cannot register plugins with reserved arguments', function() {
        var registeredValidator = validator.register(['watchfield'], function(){});
        ok(!registeredValidator, 'Validator registration failed when a plugin with a reserved argument was registered');
    });

    test('Custom error messages display correctly', function() {
        var maxValue = 20;
        var customMessage = 'Custom message, needs to be less than ';
        var customMessageUnformatted = customMessage + '{0}';
        var customMessageFormatted = customMessage + maxValue;

        var $input = this.makeValidationInput({
            'data-aui-validation-max': maxValue,
            'data-aui-validation-max-msg': customMessageUnformatted
        }, $('#qunit-fixture'));


        this.typeMessage($input, 'Invalid number');
        ok(this.fieldIsInvalid($input), 'Field becomes invalid after typing an invalid number with a custom message');

        this.typeMessage($input, '21');
        strictEqual(this.firstTipsyOnPageMessage(), customMessageFormatted);
    });

    test('Errors and info notifications stack correctly', function() {

        var infoMessage = 'Enter a number lower than 20';
        var errorMessage = 'Must be a number lower than 20';

        var $input = this.makeValidationInput({
            'data-aui-validation-max': 20,
            'data-aui-validation-max-msg': errorMessage,
            'data-aui-notification-info': infoMessage
        }, $('#qunit-fixture'));

        $input.focus();
        strictEqual(this.firstTipsyOnPageMessage(), infoMessage);
        this.typeMessage($input, '21');
        strictEqual(this.firstTipsyOnPageMessage(), errorMessage);
        this.typeMessage($input, '19');
        strictEqual(this.firstTipsyOnPageMessage(), infoMessage);
    });

    test('Fields that validate slowly show success indicators when they are validated', function() {
        validator.register(['slowvalidator'], function(field) {
            setTimeout(function(){
                field.validate();
            }, 2000);
        });


        var $input = this.makeValidationInput({
            'data-aui-validation-slowvalidator': ''
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, '1234');
        this.clock.tick(1000);
        strictEqual(this.successIconsOnPage(), 0, 'No success icon appears before a slow validator finishes.');
        this.clock.tick(3000);
        strictEqual(this.successIconsOnPage(), 1, 'A success icon appears after a slow validator finishes.');
    });

    test('Fields that validate slowly show errors when they are invalidated', function() {
        validator.register(['slowinvalidator'], function(field) {
            setTimeout(function(){
                field.invalidate();
            }, 2000);
        });


        var $input = this.makeValidationInput({
            'data-aui-validation-slowinvalidator': ''
        }, AJS.$('#qunit-fixture'));

        this.typeMessage($input, '1234');
        this.clock.tick(1000);
        strictEqual(this.errorIconsOnPage(), 0, 'No error messages before a slow field is invalidated');
        this.clock.tick(3000);
        strictEqual(this.errorIconsOnPage(), 1, 'An error icon appears after an invalidating slow validator finishes.');
        strictEqual(this.successIconsOnPage(), 0, 'No success icon appears after an invalidating slow validator finishes.');

    });

    module('Form validation provided validators test', {
        makeValidationInput: makeValidationInput,
        typeMessage: typeMessage,
        fieldIsValid: fieldIsValid,
        fieldIsInvalid: fieldIsInvalid,
        fieldIsUnvalidated: fieldIsUnvalidated,
        tipsyOnPage: tipsyOnPage,
        firstTipsyOnPageMessage: firstTipsyOnPageMessage,
        iconsOnPage: iconsOnPage,
        setup: function() {
            AJS.I18n.keys = {
                'aui.validation.message.matchingfield': 'Shim {0} message {1}',
                'aui.validation.message.matchingfield-novalue': 'Shim {0} no value message {1}'
            };
        },
        teardown: function(){
            $('.tipsy').remove();
        },
        setupPasswordFields: function() {
            var $password1 = $('<input type="password" id="password1">').appendTo(AJS.$('#qunit-fixture'));
            var $password2 = this.makeValidationInput({
                'data-aui-validation-watch': 'password1',
                'data-aui-validation-matchingfield': 'password1'
            }, $('#qunit-fixture'), '<input type="password">');

            return {
                $password1: $password1,
                $password2: $password2
            };
        },
        setupPasswordAndTextFields: function() {
            var $textField = $('<input type="text" id="textField">');
            $('#qunit-fixture').append($textField);

            var $passwordField = this.makeValidationInput({
                'data-aui-validation-watch': 'textField',
                'data-aui-validation-matchingfield': 'textField'
            }, $('#qunit-fixture'), '<input type="password">');

            return {
                $textField: $textField,
                $passwordField: $passwordField
            };
        },
        setupTextFields: function() {
            var $textField1 = $('<input type="text" id="textField">');
            $('#qunit-fixture').append($textField1);

            var $textField2 = this.makeValidationInput({
                'data-aui-validation-watch': 'textField',
                'data-aui-validation-matchingfield': 'textField'
            }, $('#qunit-fixture'), '<input type="text">');

            return {
                $textField1: $textField1,
                $textField2: $textField2
            };
        }
    });

    test('Matching field validator should not leak password values', function() {
        var fields = this.setupPasswordFields();

        this.typeMessage(fields.$password1, 'password123');
        this.typeMessage(fields.$password2, 'password456');


        ok(this.tipsyOnPage(), 'There is a tipsy on the page after typing mismatched passwords');

        var message = this.firstTipsyOnPageMessage();
        var messageContainsPassword1 = message.indexOf('password123') !== -1;
        var messageContainsPassword2 = message.indexOf('password456') !== -1;

        ok(!messageContainsPassword1, 'The message does not contain the first password');
        ok(!messageContainsPassword2, 'The message does not contain the second password');
    });

    test('Matching field validator should not leak password values when one field is a password and the other is not', function() {
        var fields = this.setupPasswordAndTextFields();

        this.typeMessage(fields.$textField, 'inputA');
        this.typeMessage(fields.$passwordField, 'inputB');


        ok(this.tipsyOnPage(), 'There is a tipsy on the page after typing mismatched inputs');

        var message = this.firstTipsyOnPageMessage();
        var messageContainsPassword1 = message.indexOf('inputA') !== -1;
        var messageContainsPassword2 = message.indexOf('inputB') !== -1;

        ok(!messageContainsPassword1, 'The message does not contain the first input');
        ok(!messageContainsPassword2, 'The message does not contain the second input');
    });

    test('Matching field validator messages should contain the contents of both messages', function() {
        var fields = this.setupTextFields();

        this.typeMessage(fields.$textField1, 'inputA');
        this.typeMessage(fields.$textField2, 'inputB');

        var message = this.firstTipsyOnPageMessage();
        var messageContainsInput1 = message.indexOf('inputA') !== -1;
        var messageContainsInput2 = message.indexOf('inputB') !== -1;

        ok(messageContainsInput1, 'The message contains the first input');
        ok(messageContainsInput2, 'The message contains the second input');
    });
});
