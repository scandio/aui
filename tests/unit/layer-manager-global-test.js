define(['layer-manager-global', 'aui-qunit'], function() {
    'use strict';

    function createLayer (blanketed, modal, persistent) {
        var $el = AJS.$('<div></div>').addClass('aui-layer').attr('aria-hidden', 'true').appendTo('#qunit-fixture');
        if (blanketed) {
            $el.attr('data-aui-blanketed', true);
        }

        if (modal) {
            $el.attr('data-aui-modal', true);
        }

        if (persistent) {
            $el.attr('data-aui-persistent', true);
        }

        return $el;
    }

    function createBlanketedLayer () {
        return createLayer().attr('data-aui-blanketed', 'true');
    }

    function pressEsc () {
        AJS.qunit.pressKey(AJS.keyCode.ESCAPE);
    }

    function click (el) {
        AJS.qunit.click(el);
    }

    module('Layer Manager (Global)', {
        setup: function() {
            AJS.LayerManager.global = new AJS.LayerManager();
            this.dimSpy = sinon.spy(AJS, 'dim');
            this.undimSpy = sinon.spy(AJS, 'undim');
            this.layerManagerPopTopSpy = sinon.stub(AJS.LayerManager.global, 'popTopIfNonPersistent');
            this.layerManagerPopUntilTopBlanketedSpy = sinon.stub(AJS.LayerManager.global, 'popUntilTopBlanketed');
        },
        teardown: function() {
            AJS.qunit.removeLayers();
            this.layerManagerPopTopSpy.restore();
            this.layerManagerPopUntilTopBlanketedSpy.restore();
            this.dimSpy.restore();
            this.undimSpy.restore();
        }
    });

    test('Pressing ESC hides the layer', function() {
        var $el = createLayer();

        AJS.LayerManager.global.push($el);
        pressEsc();
        ok(!AJS.layer($el).isVisible());
    });

    test('Pressing ESC hides the top layer only', function() {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer();

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);

        pressEsc();

        ok(AJS.layer($layer1).isVisible(), 'Bottom layer should be visible.');
        ok(!AJS.layer($layer2).isVisible(), 'Top layer should not be visible.');
    });

    test('Clicking blanket calls popUntilTopBlanketed', function() {
        var $layer1 = createLayer();
        var $layer2 = createLayer(true);

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);

        click('.aui-blanket');

        ok(this.layerManagerPopUntilTopBlanketedSpy.calledOnce);
    });

    test('Pressing ESC hides the non modal/persistent layers', function() {
        var $modalLayer = createLayer(false, true);
        var $persistentLayer = createLayer(false, false, true);
        var $layer1 = createLayer();

        AJS.LayerManager.global.push($modalLayer);
        AJS.LayerManager.global.push($persistentLayer);
        AJS.LayerManager.global.push($layer1);

        pressEsc();
        pressEsc();

        ok(AJS.layer($modalLayer).isVisible(), 'Modal layer should be visible');
        ok(AJS.layer($persistentLayer).isVisible(), 'Persistent layer should be visible');
        ok(!AJS.layer($layer1).isVisible(), 'Top layer should not be visible');
    });

    test('Pressing ESC hides the first non modal/persistent layer, but not under the modal blanket', function() {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer(true, true);
        var $persistentLayer = createLayer(false, false, true);
        var $layer3 = createLayer();

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);
        AJS.LayerManager.global.push($persistentLayer);
        AJS.LayerManager.global.push($layer3);

        pressEsc();
        pressEsc();

        ok(AJS.layer($layer1).isVisible(), 'Bottom layer should still be visible');
        ok(AJS.layer($layer2).isVisible(), 'Blanketed Modal layer should still be visible');
        ok(!AJS.layer($layer3).isVisible(), 'Top layer should not be visible');
        ok(AJS.layer($persistentLayer).isVisible(), 'Persistent layer should be visible');
    });

    test('Pressing ESC hides the first non modal/persistent layers and any layers ontop as a result', function() {
        var $layer1 = createLayer(true);
        var $layer2 = createLayer(true, true);
        var $layer3 = createLayer(true);
        var $modalLayer = createLayer(false, true);
        var $persistentLayer = createLayer(false, false, true);

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);
        AJS.LayerManager.global.push($layer3);
        AJS.LayerManager.global.push($modalLayer);
        AJS.LayerManager.global.push($persistentLayer);

        pressEsc();
        pressEsc();

        ok(AJS.layer($layer1).isVisible(), 'Bottom layer should still be visible');
        ok(AJS.layer($layer2).isVisible(), 'Blanketed Modal layer should still be visible');
        ok(!AJS.layer($layer3).isVisible(), 'Top layer should not be visible');
        ok(!AJS.layer($modalLayer).isVisible(), 'Persistent layer should be visible');
        ok(!AJS.layer($persistentLayer).isVisible(), 'Persistent layer should be visible');
    });

    test('Pressing ESC stops at the first blanket, with top modal', function() {
        var $layer1 = createLayer(true, false);
        var $layer2 = createLayer(true, true);
        var $layer3 = createLayer(false, true);

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);
        AJS.LayerManager.global.push($layer3);

        pressEsc();

        ok(AJS.layer($layer1).isVisible(), 'Bottom Modal layer should still be visible');
        ok(AJS.layer($layer2).isVisible(), 'Middle Blanketed Modal layer should still be visible');
        ok(AJS.layer($layer3).isVisible(), 'Top Modal layer should still be visible');
    });

    test('Clicking blanket should hide the layer', function() {
        var $el = createBlanketedLayer();

        AJS.LayerManager.global.push($el);
        click('.aui-blanket');
        ok(this.layerManagerPopUntilTopBlanketedSpy.calledOnce);
    });

    test('clicking anywhere outside of the top layer should close it', function() {
        var $layer = createLayer();

        AJS.LayerManager.global.push($layer);
        click(document);
        ok(!AJS.layer($layer).isVisible());
    });

    test('clicking outside all layers should close all layers', function() {
        var $layer1 = createLayer();
        var $layer2 = createLayer();

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);

        click(document);

        ok(!AJS.layer($layer1).isVisible(), 'Bottom layer should not be visible');
        ok(!AJS.layer($layer2).isVisible(), 'Top layer should not be visible');
    });

    test('clicking outside all layers should close all non modal/persistent layers', function() {
        var $layer1 = createLayer();
        var $modalLayer = createLayer(false, true);
        var $layer2 = createLayer();
        var $persistentLayer = createLayer(false, false, true);

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($modalLayer);
        AJS.LayerManager.global.push($layer2);
        AJS.LayerManager.global.push($persistentLayer);

        click(document);

        ok(!AJS.layer($layer1).isVisible(), 'Bottom layer should not be visible');
        ok(AJS.layer($modalLayer).isVisible(), 'Modal layer should still be visible');
        ok(!AJS.layer($layer2).isVisible(), 'layer 2 should not be visible');
        ok(AJS.layer($persistentLayer).isVisible(), 'Persistent layer should be visible');
    });

    test('Clicking a layer should close all layers above it if not modal/persistent', function() {
        var $modalLayer1 = createLayer(false, true);
        var $layer2 = createLayer(true);
        var $modalLayer2 = createLayer(false, true);
        var $layer3 = createLayer();

        AJS.LayerManager.global.push($modalLayer1);
        AJS.LayerManager.global.push($layer2);
        AJS.LayerManager.global.push($modalLayer2);
        AJS.LayerManager.global.push($layer3);

        click($layer2[0]);

        ok(AJS.layer($modalLayer2).isVisible(), 'Modal layer 1 should be visible.');
        ok(AJS.layer($layer2).isVisible(), 'Layer 2 should not be visible.');
        ok(AJS.layer($modalLayer2).isVisible(), 'Modal layer 2 should be visible.');
        ok(!AJS.layer($layer3).isVisible(), 'Layer 3 layer should be visible.');
    });

    test('Calling popUntil() triggers the beforeHide event on each layer.', function () {
        var calledBeforeHide = false;
        var calledHide = false;
        var $layer1 = createLayer(true);
        var $layer2 = createLayer();

        AJS.LayerManager.global.push($layer1);
        AJS.LayerManager.global.push($layer2);

        AJS.layer($layer2).on('beforeHide', function () {
            calledBeforeHide = true;
        });

        $layer2.get(0).addEventListener('aui-layer-hide', function () {
            calledHide = true;
        });

        AJS.LayerManager.global.popUntil($layer1);

        ok(calledBeforeHide, 'Before hide not called');
        ok(calledHide, 'Hide not called');
        ok(!AJS.layer($layer1).isVisible(), 'Layer 1 should not be visible.');
        ok(!AJS.layer($layer2).isVisible(), 'Layer 2 should not be visible.');
    });
});
